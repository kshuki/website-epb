﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
	public class Material
	{
		public string Title;
		public TypeSteel type;
		public uint PermissibleStresses;
		public bool isCustomMaterial;
	}

	public enum TypeSteel
	{
		carbon,
		alloy,
		austenitic
	}
}
