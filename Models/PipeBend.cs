﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
	public class PipeBend : Material
	{
		public uint Id;
		public uint IdPipe;
		public int Pressure;
		public int Temperature;
		public uint OutsideDiameter;
		public uint CurrentThickness;
		public uint RejectionThickness;
		public uint RadiusBend;
		public uint Ovality;
		public uint WeldСoefficient;
		public uint WearRate;
		public uint Resource;
	}
}