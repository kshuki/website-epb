﻿namespace Models
{
	public class PipeStraight : Material
	{
		public uint Id { get; set; }
		public uint IdPipe { get; set; }

		public float Pressure { get; set; }
		public float Temperature { get; set; }
		public float OutsideDiameter { get; set; }

		public float CurrentThickness { get; set; }
		public float RejectionThickness { get; set; }
		public float EstimatedThickness { get; set; }

		public float WearRate { get; set; }
		public float WeldСoefficient { get; set; }

		public bool IsCustomMaterial { get; set; }
		public string TitleMaterial { get; set; }
		public float PermissibleStresses { get; set; }  //Допускаемые напряжения
		public uint Resource { get; set; }              //Отработанный ресурс элемента	

		public uint RemainingServiceLife { get; set; }  //Остаточный срок службы
	}
}
