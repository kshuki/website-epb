﻿using System.Collections.Generic;

namespace Models
{
	public class Profile
	{
		public uint Id { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public bool IsAdmin { get; set; }
	}
}
