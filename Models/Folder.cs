﻿namespace Models
{
	public class Folder
	{
		public uint Id { get; set; }
		public uint IdProfile { get; set; }
		public uint IdParent { get; set; }
		public string Title { get; set; }
	}
}
