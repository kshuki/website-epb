﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
	public class Pipe
	{
		public uint Id { get; set; }
		public uint IdProfile{ get; set; }
		public uint IdParent { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		//public int Think { get; set; }
		//public PipeType Type { get; set; }
		//public string Material { get; set; }
		public bool IsBuy { get; set; }
	}

	//public enum PipeType
	//{
	//	line,
	//	bend
	//}
}
