﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Security.Claims;
using System.Threading.Tasks;
using WebsiteEPB.ViewModels;
using Microsoft.AspNetCore.Http;
using DBRepository.Interfaces;

namespace WebsiteEPB.Controllers
{
	[Route("api/[controller]")]
	public class FolderController : Controller
	{
		private readonly IObjectTree<Folder> _folders;
		private readonly IObjectTree<Pipe> _pipes;
		private readonly IProfile<Profile> _profiles;

		public FolderController(IProfile<Profile> users, IObjectTree<Folder> folders, IObjectTree<Pipe> pipes)
		{
			_folders = folders;
			_profiles = users;
			_pipes = pipes;
		}

		[Authorize]
		[Route("getfolders")]
		[HttpPost]
		public async Task<IActionResult> GetFolders()
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			//var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
			var folders = _folders.GetObjects(currentUser.Id);
			return Ok(folders);
		}

		[Authorize]
		[Route("removefolder")]
		[HttpPost]
		public async Task<IActionResult> RemoveFolder([FromBody] Folder folder)
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			var response = _folders.RemoveObject(folder.Id, currentUser.Id);

			if (response)
				return Ok();
			else
				return NotFound();
		}

		[Authorize]
		[Route("addfolder")]
		[HttpPost]
		public async Task<IActionResult> AddFolder([FromBody] CreateObjectViewMode model)
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			var folder = _folders.AddObject(currentUser.Id, model.IdParent, model.Title);
			return Ok(folder);
		}

		[Authorize]
		[Route("getfolder")]
		[HttpPost]
		public async Task<IActionResult> GetFolder(uint idFolder)
		{
			var folder = _folders.GetObject(idFolder);
			return Ok(folder);
		}


		[Authorize]
		[Route("changefolder")]
		[HttpPost]
		public async Task<IActionResult> ChangeFolder([FromBody] Folder folder)
		{
			//var bdFolder = _folders.GetObject(folder.Id);
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			folder.IdProfile = currentUser.Id;

			var response = _folders.EditObject(folder);

			if (response)
				return Ok();
			else
				return NotFound();
		}
	}
}
