﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Security.Claims;
using System.Threading.Tasks;
using WebsiteEPB.ViewModels;
using Microsoft.AspNetCore.Http;
using DBRepository.Interfaces;

namespace WebsiteEPB.Controllers
{
	[Route("api/[controller]")]
	public class PipeController : Controller
	{
		
		private readonly IProfile<Profile> _profiles;
		private readonly IObjectTree<Pipe> _pipes;
		private readonly IPipePart<PipeStraight> _pipeStraights;

		public PipeController(IProfile<Profile> users, IPipePart<PipeStraight> pipeStraights, IObjectTree<Pipe> pipes)
		{
			_profiles = users;
			_pipeStraights = pipeStraights;
			_pipes = pipes;
		}

		[Authorize]
		[Route("getpipes")]
		[HttpPost]
		public async Task<IActionResult> GetPipes()
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			//var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
			var pipes = _pipes.GetObjects(currentUser.Id);
			return Ok(pipes);
		}

		[Authorize]
		[Route("removepipe")]
		[HttpPost]
		public async Task<IActionResult> RemovePipe([FromBody] Pipe pipe)
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			var response = _pipes.RemoveObject(pipe.Id, currentUser.Id);

			if (response)
				return Ok();
			else
				return NotFound();
		}

		[Authorize]
		[Route("addpipe")]
		[HttpPost]
		public async Task<IActionResult> AddPipe([FromBody] CreateObjectViewMode model)
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			var pipe = _pipes.AddObject(currentUser.Id, model.IdParent, model.Title);
			return Ok(pipe);
		}

		[Authorize]
		[Route("getpipe")]
		[HttpPost]
		public async Task<IActionResult> GetPipe([FromBody] uint pipeId)
		{
			var pipeStraights = _pipeStraights.GetObjects(pipeId);

			var pipe = _pipes.GetObject(pipeId);

			PipeViewModel pipeView = new PipeViewModel{
				PipeLine = pipe,
				PipeStraights = pipeStraights
            };

			return Ok(pipeView);
		}


		[Authorize]
		[Route("changepipe")]
		[HttpPost]
		public async Task<IActionResult> ChangePipe([FromBody] Pipe pipe)
		{
			var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
			pipe.IdProfile = currentUser.Id;

			var response = _pipes.EditObject(pipe);

			if (response)
				return Ok();
			else
				return NotFound();
		}


		[Authorize]
		[Route("addpipestraight")]
		[HttpPost]
		public async Task<IActionResult> AddPipeStraight([FromBody] PipeStraight pipeStraight)
		{
			var newPipeStraight = _pipeStraights.AddObject(pipeStraight);

			if (newPipeStraight != null)
				return Ok(newPipeStraight);
			else
				return NotFound();
		}

		[Authorize]
		[Route("changepipestraight")]
		[HttpPost]
		public async Task<IActionResult> ChangePipeStraight([FromBody] PipeStraight pipeStraight)
		{
			var response = _pipeStraights.EditObject(pipeStraight);

			if (response)
				return Ok();
			else
				return NotFound();
		}
	}
}
