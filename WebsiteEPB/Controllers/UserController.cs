﻿using DBRepository.Interfaces;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using WebsiteEPB.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;

namespace WebsiteEPB.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IProfile<Profile> _profiles;

        public UserController(IProfile<Profile> users)
        {
            _profiles = users;
        }

        [Authorize]
        public IActionResult Index()
        {
            ProfileViewModel profile = new ProfileViewModel();
            profile.CurrentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
            return View(profile);
        }

        [Route("profile")]
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            ProfileViewModel profile = new ProfileViewModel();
            var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));

            if (currentUser != null)
            {
                profile.InstalProfile(currentUser);
                return Ok(profile);
            }
            else 
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return Unauthorized();
            }
        }

        [Route("login")]
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model) //Task<string> //string email, string password, bool remember
        {
            var user = _profiles.GetProfile(model.Email);
            if (user != null && user.Password == model.Password)
            {
                //var now = DateTime.UtcNow;
                //var identity = await Authenticate(model.Email, model.RememberMe);
                //// создаем JWT-токен
                //var jwt = new JwtSecurityToken(
                //        issuer: AuthOptions.ISSUER,
                //        audience: AuthOptions.AUDIENCE,
                //        notBefore: now,
                //        claims: identity.Claims,
                //        expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                //        signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
                //var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
                await Authenticate(model.Email, model.RememberMe);
                ProfileViewModel profile = new ProfileViewModel();
                profile.InstalProfile(user);
                return Ok(profile);
            }
            else
            {   
                return Unauthorized();
            }
        }

        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            var user = _profiles.GetProfile(model.Email);
            if (user == null)
            {
                var newUser = new Profile
                {
                    FirstName = model.FirstName,
                    SecondName = model.SecondName,
                    Email = model.Email,
                    Password = model.Password
                };
                _profiles.AddProfile(newUser);
                await Authenticate(model.Email, true); 
                return Ok();
                //return RedirectToAction("Index", "Home");
            }
            else
            {
                return Unauthorized("Такой e-mail уже зарегестрирован");
            }
        }

        [Route("editprofile")]
        [HttpPatch]
        public async Task<IActionResult> EditProfile(){
            ProfileViewModel profile = new ProfileViewModel();
            var currentUser = _profiles.GetProfile(HttpContext.User.FindFirstValue(ClaimTypes.Name));
            if (currentUser != null)
            {
                profile.InstalProfile(currentUser);
                return Ok(profile);
            }
            else 
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return Unauthorized();
            }
        }

        //[Route("users")]
        [HttpGet]
        public IEnumerable<Profile> UserList() //TODO: заменить на acync Task<IEnumerable<User>> 
        {
            //UserListViewModel usersModel = new UserListViewModel();
            //usersModel.GetUsers = _users.Objects;
            return _profiles.GetProfiles;
            //return View(usersModel);
        }

        [Route("logout")]
        [HttpGet]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
           await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
           return Ok();
        }

        private async Task Authenticate(string email, bool rememberMe)
        {
            var sha256 = new SHA256Managed();

            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, email)
            };
            var authProps = new AuthenticationProperties
            {
                AllowRefresh = rememberMe,
                IsPersistent = rememberMe
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id), authProps);
        }
    }
}