using DBRepository.Interfaces;
using DBRepository.Mocks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Models;
using System;
using WebsiteEPB.Helpers;
using Microsoft.IdentityModel.Tokens;

namespace WebsiteEPB
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }
		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(options =>
				{
					options.RequireHttpsMetadata = false;
					options.SaveToken = true;
					options.TokenValidationParameters = new TokenValidationParameters
					{
						ValidIssuer = AuthOptions.ISSUER,
						ValidAudience = AuthOptions.AUDIENCE,
						IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
						ValidateLifetime = true,
						ValidateIssuerSigningKey = true,
						ClockSkew = TimeSpan.Zero
					};
				});

			services.AddScoped<IProfile<Profile>, MockProfiles>();
			services.AddScoped<IObjectTree<Folder>, MockFolders>();
			services.AddScoped<IObjectTree<Pipe>, MockPipe>();
			services.AddScoped<IPipePart<PipeStraight>, MockPipeStraight>();
			//services.AddScoped<IObject<Folder>, MockFolders>();
			//services.AddScoped<IIdentityRepository, MockUsers>();

			services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
				//.AddCookie();
				.AddCookie(options => //CookieAuthenticationOptions
				{
					options.LogoutPath = new Microsoft.AspNetCore.Http.PathString("/api/User/Logout");
					options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/api/User/Login");
					options.Cookie.Name = "epb_token";
				});

			services.AddDistributedMemoryCache();

			//services.AddSession();
			// services.AddSession(options =>
			// {
			//     // Set a short timeout for easy testing.
			//     options.IdleTimeout = TimeSpan.FromSeconds(10);
			//     options.Cookie.HttpOnly = true;
			//     // Make the session cookie essential
			//     options.Cookie.IsEssential = true;
			// });

			services.AddMvc(options => options.EnableEndpointRouting = false);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		[System.Obsolete]
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				//app.UseDeveloperExceptionPage();
				app.UseStatusCodePages();
				app.UseWebpackDevMiddleware();
			}

			app.UseStaticFiles();
			//app.UseSession();
			app.UseAuthentication();
			app.UseAuthorization();

			//app.UseMvcWithDefaultRoute();
			//app.UseRouting();
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "DefaultApi",
					template: "api/{controller}/{action}");
				routes.MapSpaFallbackRoute("spa-fallback", new { controller = "Home", action = "Index" });
			});
		}
	}
}
