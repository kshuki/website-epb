﻿using Models;
using System.Collections.Generic;

namespace WebsiteEPB.ViewModels
{
	public class UserListViewModel
	{
		public IEnumerable<Profile> GetUsers { get; set; }
	}
}
