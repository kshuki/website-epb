﻿using Models;
using System.Collections.Generic;

namespace WebsiteEPB.ViewModels
{
	public class ProfileViewModel
	{
		public uint Id { get; set; }
		public Profile CurrentUser { get; set; }
		public string FirstName { get; set; }
		public string SecondName { get; set; }
		public bool IsAdmin { get; set; }

		public void InstalProfile(Profile user)
		{
			Id = user.Id;
			FirstName = user.FirstName;
			SecondName = user.SecondName;
			IsAdmin = user.IsAdmin;
		}
	}
}
