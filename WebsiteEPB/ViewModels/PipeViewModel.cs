﻿using Models;
using System.Collections.Generic;


namespace WebsiteEPB.ViewModels
{
    public class PipeViewModel
    {
        public Pipe PipeLine { get; set; }
        public IEnumerable<PipeStraight> PipeStraights { get; set; }
    }
}
