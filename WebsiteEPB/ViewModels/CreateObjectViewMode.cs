﻿namespace WebsiteEPB.ViewModels
{
	public class CreateObjectViewMode
	{
		public uint IdParent { get; set; }
		public string Title { get; set; }
	}
}
