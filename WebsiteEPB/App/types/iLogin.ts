export interface ILogin {
	isLogin: boolean,
	isLoading: boolean,
	email: string,
	password: string,
	isRememberMe: boolean
}