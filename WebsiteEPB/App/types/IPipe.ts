import { IPipeStraight } from './iPipeStraight';
import { IPipeBend } from './iPipeBend';

export interface IPipe {
	id: number,
	title: string,
	idParent: number,
	straights: Array<IPipeStraight>,
	bends: Array<IPipeBend>,
	isBuy: boolean
}