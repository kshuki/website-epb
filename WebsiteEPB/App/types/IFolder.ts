import { IPipe } from "./iPipe";

export interface IFolder {
	id: number,
	idParent: number,
	title: string,
	folders: Array<IFolder>,
	pipes: Array<IPipe>,
	isOpen?: boolean
}