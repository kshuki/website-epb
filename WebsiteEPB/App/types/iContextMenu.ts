import { IFolder } from './iFolder';
import { IPipe } from './iPipe';

export interface IContextMenu {
	folder?: IFolder,
	pipe?: IPipe,
	posX: number,
	posY: number,
	type: typeContextMenu,
}

export enum typeContextMenu {
	pipe = 'pipe',
	folder = 'folder',
	title = 'title',
	close = 'close'
}