import { IMaterialProp } from "./iMaterial";

export interface IPipeBend {
	id: number | undefined,
	pressure: number,
	temperature: number,
	outsideDiameter: number,
	thickness: number,
	// plusThickness : number, //Плюсовой допуск
	rejectionThickness: number,
	radiusBend: number,
	ovality: number,
	// corrosionAllowance: number,
	weldСoefficient: number,
	wearRate: number,
	resource: number,
	material: IMaterialProp,
	remainingServiceLife: number
}

export interface IPipeBendCalculation extends IPipeBend {
	estimatedThickness: number,
	remainingServiceLife: number,
	K1: number,
	K2: number,
	K3: number,
	Y1: number,
	Y2: number,
	Y3: number,
	Sr: number,
	Sr1: number,
	Sr2: number,
	Sr3: number,
	q: number,
	alpha: number,
}