export interface IProfile {
	id: number,
	firstName: string,
	secondName: string,
	isLoggen: boolean,
	status: string
}