export interface IError {
	cancel?: Function,
	reiterate?: Function,
	message: string;
	isOpen: boolean
}