import { IPipeStraight } from './iPipeStraight';
import { IProfile } from './iProfile';
import { IHeader } from './iHeader';
import { IError } from './iError';
import { ILoading } from './iLoading';
import { IContextMenu } from './iContextMenu';
// import { IObjectTree } from './iObjectTree';
import { IWorkplace } from './iWorkplace';

export interface IAppStore {
	profileModule: IProfile,
	pipeStraightModule: IPipeStraight,
	headerModule: IHeader,
	errorModule: IError,
	loadingModule: ILoading,
	contextMenuModule: IContextMenu,
	workplaceModule: IWorkplace,
	// objectTreeModule: IObjectTree
};