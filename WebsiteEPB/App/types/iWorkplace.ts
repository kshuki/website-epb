import { IPipe } from './iPipe';

export interface IWorkplace {
    pipe: IPipe | undefined
}