export interface IMaterialSource {
	title: string,
	type: typeSteel,
	resources: Array<IResource>
}

export interface IResource {
	resource: number
	property: Array<{ temperature: number, permissibleStresses: number }>
}

export enum typeSteel {
	carbon = 'углеродистая',
	alloy = 'легированная',
	austenitic = 'аустенитная',
}

export interface IMaterialProp {
	title: string,
	type: typeSteel,
	permissibleStresses: number,
	isCustomMaterial: boolean
}