import { IMaterialProp } from "./iMaterial";

export interface IPipeStraight {
	id: number | undefined, 
	idPipe: number,
	pressure: number,
	temperature: number,
	outsideDiameter: number,
	currentThickness: number,
	rejectionThickness: number,
	weldСoefficient: number,
	wearRate: number,
	material: IMaterialProp,
	resource: number,
	estimatedThickness: number,
	remainingServiceLife: number,
}