import { Action, Reducer } from 'redux';
import { IWorkplace, IPipe } from '../types';
import { actions } from '../actions/constants';

const initialState: IWorkplace = {
	pipe: undefined
}

export const workplace: Reducer<IWorkplace, Action> = (state = initialState, action: any) => {
	let pipe: IPipe | undefined;
	switch (action.type) {
		case actions.WORKPLACE_OPEN:
			return {
				...action.payload
			}
		case actions.WORKPLACE_CLOSE:
			pipe = state.pipe === action.payload.pipe ? undefined : state.pipe
			return {
				...state, pipe: pipe
			}
		case actions.WORKPLACE_UPDATE:
			pipe = state.pipe?.id === action.payload.pipe?.id ? action.payload.pipe : state.pipe
			// console.log(pipe);
			return {
				...state, pipe: pipe
			}
		default:
			return {
				...state
			}
	}
};
