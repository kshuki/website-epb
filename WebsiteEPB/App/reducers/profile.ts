import { actions } from './../actions/constants';
import { Action, Reducer } from 'redux';
import { IProfile } from '../types/iProfile';
// import { actions } from '../actions/constants';
//import { headerTypeState, headerEnumReducer, headerTypeDispatch } from "./headerType"


const initialState: IProfile = {
	id: 0,
	firstName: '',
	secondName: '',
	isLoggen: false,
	status: 'logout'
}


export const profile: Reducer<IProfile, Action> = (state = initialState, action: any) => {
	switch (action.type) {
		case actions.LOGIN:
			return {
				...action.payload as IProfile, isLoggen: true
			}
		case actions.LOGOUT:
			return {
				...initialState
			}
		case actions.EDIT_PROFILE:
			return {
				...state, firstName: action.payload.firstName, secondName: action.payload.secondName
			}
		default:
			return {
				...state
			}
	}
}


// function headerReducer(state = initialState, action: headerTypeDispatch): headerTypeState {
//     switch (action.type) {
//         case headerEnumReducer.login:
//             return { ...state, isLogged: true, isLoading: false, name: action.name!! }

//         case headerEnumReducer.logout:
//             return { ...state, isLogged: false, isLoading: true }

//         // case headerEnumReducer.logout:
//         //     return { ...state, error: action.payload }

//         // case LOGOUT:
//         //     return { ...state, isLogged: false, name: '', password: '' }

//         // case SHOW_LOGIN_FORM:
//         //     return {...state, isLoginFormShowed: action.payload }

//         // case INPUT_LOGIN:
//         //     return {...state, name: action.payload }

//         // case INPUT_PASSWORD:
//         //     return {...state, password: action.payload }

//         default:
//             return state;
//     }
// }

// export { initialState, headerReducer };