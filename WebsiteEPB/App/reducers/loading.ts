import { Action, Reducer } from 'redux';
import { ILoading } from '../types';
import { actions } from '../actions/constants';

const initialState: ILoading = {
	isLoading: false,
}

export const loading: Reducer<ILoading, Action> = (state = initialState, action: any) => {
	switch (action.type) {
		case actions.LOADING_START:
			return {
				...state, isLoading: true
			}
		case actions.LOADING_STOP:
			return {
				...state, isLoading: false
			}
		default:
			return {
				...state
			}
	}
};