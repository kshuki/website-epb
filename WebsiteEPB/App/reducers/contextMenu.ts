import { Action, Reducer } from 'redux';
import { IContextMenu, typeContextMenu, IFolder, IPipe } from '../types';
import { actions } from '../actions/constants';

const initialState: IContextMenu = {
	folder: {
		id: 0,
		idParent: 0,
		title: "",
		folders: [],
		pipes: []
	},
	posX: 0,
	posY: 0,
	type: typeContextMenu.close
}

const getStateFolder = (params: { folder: IFolder, posX: number, posY: number }): IContextMenu => {
	return ({ ...params, type: typeContextMenu.folder });
}

const getStatePipe = (params: { pipe: IPipe, posX: number, posY: number }): IContextMenu => {
	return ({ ...params, type: typeContextMenu.pipe });
}

const getStateTitle = (params: { posX: number, posY: number }): IContextMenu => {
	return ({ ...params, type: typeContextMenu.title });
}


export const contextMenu: Reducer<IContextMenu, Action> = (state = initialState, action: any) => {
	switch (action.type) {
		case actions.OPEN_CONTEXT_MENU_FOLDER:
			return {
				...getStateFolder(action.payload),
			}
		case actions.OPEN_CONTEXT_MENU_PIPE:
			return {
				...getStatePipe(action.payload),
			}
		case actions.OPEN_CONTEXT_MENU_TITLE_TREE:
			return {
				...getStateTitle(action.payload),
			}
		case actions.CLOSE_CONTEXT_MENU:
			return {
				...state, type: typeContextMenu.close
			}
		default:
			return {
				...state
			}
	}
};