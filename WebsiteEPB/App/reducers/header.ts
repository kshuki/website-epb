import { Action, Reducer } from 'redux';
import { IHeader, IProfile } from '../types';
import { actions } from '../actions/constants';

const initialState: IHeader = {
	isLogin: false,
	name: ''
}

const getName = (profile: IProfile) => {
	return profile.firstName + ' ' + profile.secondName;
}

export const header: Reducer<IHeader, Action> = (state = initialState, action: any) => {
	switch (action.type) {
		case actions.LOGIN:
			return {
				...state, isLogin: true, name: getName(action.payload)
			}
		case actions.LOGOUT:
			return {
				...initialState
			}
		default:
			return {
				...state
			}
	}
};
