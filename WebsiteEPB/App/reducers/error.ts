import { Action, Reducer } from 'redux';
import { IError, IProfile } from '../types';
import { actions } from '../actions/constants';

const initialState: IError = {
	isOpen: false,
	message: ''
}

// const getName = (profile: IProfile) => {
// 	return profile.firstName + ' ' + profile.secondName;
// }

const getState = (params: { cancel?: Function, message: string, reiterate?: Function }): IError => {
	return ({ ...params, isOpen: true });
}

export const error: Reducer<IError, Action> = (state = initialState, action: any) => {
	switch (action.type) {
		case actions.ERROR_OPEN:
			return {
				...getState(action.payload)
			}
		case actions.ERROR_CLOSE:
			return {
				...initialState
			}
		default:
			return {
				...state
			}
	}
};