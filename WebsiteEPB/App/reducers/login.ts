import { LINKS } from './../constants/routes';
import { useHistory } from 'react-router-dom';
import { Action, Reducer } from 'redux';
import { ILogin, IProfile } from '../types';
import { actions } from '../actions/constants';

const initialState: ILogin = {
	isLogin: false,
	isLoading: false,
	email: '',
	password: '',
	isRememberMe: false
}

const redirect = () => {
	let history = useHistory();
	history.push(LINKS.MAIN);
}

export const header: Reducer<ILogin, Action> = (state = initialState, action: any) => {

	switch (action.type) {
		case actions.LOGIN:
			redirect();
			return {
				...state
			}
		default:
			return {
				...state
			}
	}
};