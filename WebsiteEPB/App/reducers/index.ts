import { error } from './error';
import { header } from './header';
import { loading } from './loading'
import { profile } from './profile';
import { workplace } from './workplace';
import { contextMenu } from './contextMenu';
import { combineReducers } from 'redux';



const reducers = combineReducers({
	profileModule: profile,
	headerModule: header,
	errorModule: error,
	loadingModule: loading,
	contextMenuModule: contextMenu,
	workplaceModule: workplace,
})

export default reducers;