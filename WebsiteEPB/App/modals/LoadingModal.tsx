import React from 'react';
import { ILoading, IAppStore } from '../types';
import { useSelector } from 'react-redux';

const LoadingModal = () => {
	const loadModule: ILoading = useSelector((state: IAppStore) => state.loadingModule);

	if (!loadModule.isLoading) {
		return (<></>)
	}

	return (
		<div className="modal_container">
			окно загрузки
		</div>
	)
}

export default LoadingModal;