import React from 'react';
import { IError, IAppStore } from '../types';
import { useSelector, useDispatch } from 'react-redux';
import { actions } from '../actions/constants';

const ErrorModal = () => {
	const errorModule: IError = useSelector((state: IAppStore) => state.errorModule);
	const dispatch = useDispatch();

	const closeErrorModal = () => {
		if (errorModule.cancel != void (0)) {
			errorModule.cancel();
		}
		dispatch({ type: actions.ERROR_CLOSE })
	}

	if (!errorModule.isOpen) {
		return (<></>)
	}

	return (
		<div className="modal_container">
			<div className="modal_window">
				<div className="modal_text">
					<h2>Ошибка</h2>
					{errorModule.message}
				</div>
				{
					errorModule.reiterate 
					? 
						<div className="modal_buttons" style={{ justifyContent: 'space-between' }}>
							<button onClick={() => errorModule.reiterate!()}>Повторить</button>
							<button onClick={closeErrorModal}>Отмена</button>
						</div>
					:
						<div className="modal_buttons" style={{ justifyContent: 'space-around' }}>
							<button onClick={closeErrorModal}>Ок!</button>
						</div>
				}
			</div>
		</div>
	)
}

export default ErrorModal;