import React from 'react';

const InputPipe = ({ type, placeholder, name, value, onChange }: { type?: string, placeholder?: string, name: string, value?: string, onChange: (value: string | number) => void }) => {
	return (
		<input
			type={type ? type : 'number'}
			placeholder={placeholder ? placeholder : ''}
			name={name}
			value={value}
			onChange={(event) => { onChange(event.target.value) }}
			required
		/>
	)
}

export { InputPipe };