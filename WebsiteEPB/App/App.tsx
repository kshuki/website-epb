import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, BrowserRouter } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import HeaderContainer from './containers/HeaderContainer';
import RouteContainer from './containers/RouteContainer';
import FooterContainer from './containers/FooterContainer';
import ErrorModal from './modals/ErrorModal'
import LoadingModal from './modals/LoadingModal';
import { headerLogin, API } from './constants';
import { IProfile } from './types';
import { actions } from './actions/constants';
import './source/scss/style.scss';

require("babel-core/register");
require("babel-polyfill");

const App: React.FC = () => {
	const [loading, setLoading] = useState(true);

	if (loading) {
		getProfile(() => setLoading(false));
		return (<div>Please wait</div>);
	} else {
		return (
			<Router>
				<div className="wrapper">
					<HeaderContainer />
					<RouteContainer />
					<FooterContainer />
					<ErrorModal />
					<LoadingModal />
				</div>
			</Router>
		)
	}
}

export default App;


function getProfile(stopLoad: (() => void)) {
	const handleApp = useDispatch();
	axios.get(API.GET_PROFILE, headerLogin())
		.then((response: { status: number, data: IProfile }) => {
			// console.log(response);
			handleApp({ type: actions.LOGIN, payload: response.data })
			stopLoad();
			return;
		})
		.catch(error => {
			console.log('app', error);
			handleApp({ type: actions.LOGOUT });

			// logout();
			stopLoad();
			return;
		})
}