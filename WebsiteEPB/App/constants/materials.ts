import { IMaterialSource, IResource, typeSteel } from './../types';

//допускаемые напряжения для материалов не требующих ресурсов
const m_St2kpResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 124 },
			{ temperature: 150, permissibleStresses: 106 },
			{ temperature: 200, permissibleStresses: 95 },
			{ temperature: 250, permissibleStresses: 80 },
		]
	}
]

const m_St3kpResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 133 },
			{ temperature: 150, permissibleStresses: 115 },
			{ temperature: 200, permissibleStresses: 111 },
			{ temperature: 250, permissibleStresses: 102 },
		]
	}
]

const m_St2spSt2psResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 130 },
			{ temperature: 150, permissibleStresses: 112 },
			{ temperature: 200, permissibleStresses: 100 },
			{ temperature: 250, permissibleStresses: 86 },
			{ temperature: 275, permissibleStresses: 78 },
			{ temperature: 300, permissibleStresses: 70 },
		]
	}
]

const m_St3spSt3psResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 140 },
			{ temperature: 150, permissibleStresses: 125 },
			{ temperature: 200, permissibleStresses: 117 },
			{ temperature: 250, permissibleStresses: 107 },
			{ temperature: 275, permissibleStresses: 102 },
			{ temperature: 300, permissibleStresses: 96 },
		]
	}
]

const m_St4spSt4psResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 145 },
			{ temperature: 150, permissibleStresses: 129 },
			{ temperature: 200, permissibleStresses: 121 },
			{ temperature: 250, permissibleStresses: 111 },
			{ temperature: 275, permissibleStresses: 106 },
			{ temperature: 300, permissibleStresses: 98 },
		]
	}
]

const m_St3GspResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 150 },
			{ temperature: 150, permissibleStresses: 134 },
			{ temperature: 200, permissibleStresses: 125 },
			{ temperature: 250, permissibleStresses: 115 },
			{ temperature: 275, permissibleStresses: 109 },
			{ temperature: 300, permissibleStresses: 103 },
		]
	}
]

const m_22KResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 170 },
			{ temperature: 150, permissibleStresses: 155 },
			{ temperature: 200, permissibleStresses: 147 },
			{ temperature: 250, permissibleStresses: 140 },
			{ temperature: 275, permissibleStresses: 135 },
			{ temperature: 300, permissibleStresses: 130 },
			{ temperature: 320, permissibleStresses: 126 },
			{ temperature: 340, permissibleStresses: 122 },
			{ temperature: 350, permissibleStresses: 120 },
		]
	}
]

const m_14GNMAResources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 180 },
			{ temperature: 150, permissibleStresses: 179 },
			{ temperature: 200, permissibleStresses: 175 },
			{ temperature: 250, permissibleStresses: 171 },
			{ temperature: 275, permissibleStresses: 170 },
			{ temperature: 300, permissibleStresses: 169 },
			{ temperature: 320, permissibleStresses: 164 },
			{ temperature: 340, permissibleStresses: 161 },
			{ temperature: 350, permissibleStresses: 159 },
			{ temperature: 360, permissibleStresses: 157 },
			{ temperature: 370, permissibleStresses: 155 },
			{ temperature: 380, permissibleStresses: 152 },
		]
	}
]

const m_16GNM_16GNMA_Resources: Array<IResource> = [
	{
		resource: 0,
		property: [
			{ temperature: 50, permissibleStresses: 190 },
			{ temperature: 150, permissibleStresses: 181 },
			{ temperature: 200, permissibleStresses: 176 },
			{ temperature: 250, permissibleStresses: 172 },
			{ temperature: 275, permissibleStresses: 196 },
			{ temperature: 300, permissibleStresses: 167 },
			{ temperature: 320, permissibleStresses: 165 },
			{ temperature: 340, permissibleStresses: 163 },
			{ temperature: 350, permissibleStresses: 161 },
			{ temperature: 360, permissibleStresses: 159 },
			{ temperature: 370, permissibleStresses: 157 },
			{ temperature: 380, permissibleStresses: 154 },
		]
	}
]

const m_08_10_12K_Resources: Array<IResource> = [
	{
		resource: 10000,
		property: [
			{ temperature: 100, permissibleStresses: 130 },
			{ temperature: 200, permissibleStresses: 120 },
			{ temperature: 250, permissibleStresses: 108 },
			{ temperature: 275, permissibleStresses: 102 },
			{ temperature: 300, permissibleStresses: 96 },
			{ temperature: 320, permissibleStresses: 92 },
			{ temperature: 340, permissibleStresses: 87 },
			{ temperature: 350, permissibleStresses: 85 },
			{ temperature: 360, permissibleStresses: 82 },
			{ temperature: 380, permissibleStresses: 76 },
			{ temperature: 400, permissibleStresses: 73 },
			{ temperature: 410, permissibleStresses: 70 },
			{ temperature: 420, permissibleStresses: 68 },
			{ temperature: 430, permissibleStresses: 66 },
			{ temperature: 440, permissibleStresses: 63 },
			{ temperature: 450, permissibleStresses: 61 },
			{ temperature: 460, permissibleStresses: 58 },
			{ temperature: 470, permissibleStresses: 52 },
			{ temperature: 480, permissibleStresses: 45 },
			{ temperature: 490, permissibleStresses: 39 },
			{ temperature: 500, permissibleStresses: 33 },
			{ temperature: 510, permissibleStresses: 26 },
		]
	},
	{
		resource: 100000,
		property: [
			{ temperature: 100, permissibleStresses: 130 },
			{ temperature: 200, permissibleStresses: 120 },
			{ temperature: 250, permissibleStresses: 108 },
			{ temperature: 275, permissibleStresses: 102 },
			{ temperature: 300, permissibleStresses: 96 },
			{ temperature: 320, permissibleStresses: 92 },
			{ temperature: 340, permissibleStresses: 87 },
			{ temperature: 350, permissibleStresses: 85 },
			{ temperature: 360, permissibleStresses: 82 },
			{ temperature: 380, permissibleStresses: 76 },
			{ temperature: 400, permissibleStresses: 73 },
			{ temperature: 410, permissibleStresses: 68 },
			{ temperature: 420, permissibleStresses: 62 },
			{ temperature: 430, permissibleStresses: 57 },
			{ temperature: 440, permissibleStresses: 51 },
			{ temperature: 450, permissibleStresses: 46 },
			{ temperature: 460, permissibleStresses: 40 },
			{ temperature: 470, permissibleStresses: 34 },
			{ temperature: 480, permissibleStresses: 28 },
			{ temperature: 490, permissibleStresses: 24 },
			{ temperature: 500, permissibleStresses: 20 },
		]
	},
	{
		resource: 200000,
		property: [
			{ temperature: 100, permissibleStresses: 130 },
			{ temperature: 200, permissibleStresses: 120 },
			{ temperature: 250, permissibleStresses: 108 },
			{ temperature: 275, permissibleStresses: 102 },
			{ temperature: 300, permissibleStresses: 96 },
			{ temperature: 320, permissibleStresses: 92 },
			{ temperature: 340, permissibleStresses: 87 },
			{ temperature: 350, permissibleStresses: 85 },
			{ temperature: 360, permissibleStresses: 82 },
			{ temperature: 380, permissibleStresses: 76 },
			{ temperature: 400, permissibleStresses: 66 },
			{ temperature: 410, permissibleStresses: 61 },
			{ temperature: 420, permissibleStresses: 57 },
			{ temperature: 430, permissibleStresses: 51 },
			{ temperature: 440, permissibleStresses: 45 },
			{ temperature: 450, permissibleStresses: 38 },
			{ temperature: 460, permissibleStresses: 33 },
			{ temperature: 470, permissibleStresses: 28 },
			{ temperature: 480, permissibleStresses: 22 },
		]
	},
	{
		resource: 300000,
		property: [
			{ temperature: 100, permissibleStresses: 130 },
			{ temperature: 200, permissibleStresses: 120 },
			{ temperature: 250, permissibleStresses: 108 },
			{ temperature: 275, permissibleStresses: 102 },
			{ temperature: 300, permissibleStresses: 96 },
			{ temperature: 320, permissibleStresses: 92 },
			{ temperature: 340, permissibleStresses: 87 },
			{ temperature: 350, permissibleStresses: 85 },
			{ temperature: 360, permissibleStresses: 82 },
			{ temperature: 380, permissibleStresses: 71 },
			{ temperature: 400, permissibleStresses: 60 },
			{ temperature: 410, permissibleStresses: 55 },
			{ temperature: 420, permissibleStresses: 50 },
			{ temperature: 430, permissibleStresses: 45 },
			{ temperature: 440, permissibleStresses: 40 },
			{ temperature: 450, permissibleStresses: 35 },
			{ temperature: 460, permissibleStresses: 29 },
			{ temperature: 470, permissibleStresses: 24 },
			{ temperature: 480, permissibleStresses: 18 },
		]
	},
]

const m_15_15K_16K_Resources: Array<IResource> = [
	{
		resource: 10000,
		property: [
			{ temperature: 100, permissibleStresses: 140 },
			{ temperature: 200, permissibleStresses: 130 },
			{ temperature: 250, permissibleStresses: 120 },
			{ temperature: 275, permissibleStresses: 113 },
			{ temperature: 300, permissibleStresses: 106 },
			{ temperature: 320, permissibleStresses: 101 },
			{ temperature: 340, permissibleStresses: 96 },
			{ temperature: 350, permissibleStresses: 93 },
			{ temperature: 360, permissibleStresses: 90 },
			{ temperature: 380, permissibleStresses: 85 },
			{ temperature: 400, permissibleStresses: 80 },
			{ temperature: 410, permissibleStresses: 77 },
			{ temperature: 420, permissibleStresses: 74 },
			{ temperature: 430, permissibleStresses: 71 },
			{ temperature: 440, permissibleStresses: 68 },
			{ temperature: 450, permissibleStresses: 65 },
			{ temperature: 460, permissibleStresses: 62 },
			{ temperature: 470, permissibleStresses: 54 },
			{ temperature: 480, permissibleStresses: 46 },
			{ temperature: 490, permissibleStresses: 40 },
			{ temperature: 500, permissibleStresses: 34 },
		]
	},
	{
		resource: 100000,
		property: [
			{ temperature: 100, permissibleStresses: 140 },
			{ temperature: 200, permissibleStresses: 130 },
			{ temperature: 250, permissibleStresses: 120 },
			{ temperature: 275, permissibleStresses: 113 },
			{ temperature: 300, permissibleStresses: 106 },
			{ temperature: 320, permissibleStresses: 101 },
			{ temperature: 340, permissibleStresses: 96 },
			{ temperature: 350, permissibleStresses: 93 },
			{ temperature: 360, permissibleStresses: 90 },
			{ temperature: 380, permissibleStresses: 85 },
			{ temperature: 400, permissibleStresses: 80 },
			{ temperature: 410, permissibleStresses: 72 },
			{ temperature: 420, permissibleStresses: 66 },
			{ temperature: 430, permissibleStresses: 60 },
			{ temperature: 440, permissibleStresses: 53 },
			{ temperature: 450, permissibleStresses: 47 },
			{ temperature: 460, permissibleStresses: 40 },
			{ temperature: 470, permissibleStresses: 34 },
			{ temperature: 480, permissibleStresses: 28 },
			{ temperature: 490, permissibleStresses: 24 },
			{ temperature: 500, permissibleStresses: 20 },
		]
	},
	{
		resource: 200000,
		property: [
			{ temperature: 100, permissibleStresses: 140 },
			{ temperature: 200, permissibleStresses: 130 },
			{ temperature: 250, permissibleStresses: 120 },
			{ temperature: 275, permissibleStresses: 113 },
			{ temperature: 300, permissibleStresses: 106 },
			{ temperature: 320, permissibleStresses: 101 },
			{ temperature: 340, permissibleStresses: 96 },
			{ temperature: 350, permissibleStresses: 93 },
			{ temperature: 360, permissibleStresses: 90 },
			{ temperature: 380, permissibleStresses: 85 },
			{ temperature: 400, permissibleStresses: 72 },
			{ temperature: 410, permissibleStresses: 65 },
			{ temperature: 420, permissibleStresses: 58 },
			{ temperature: 430, permissibleStresses: 52 },
			{ temperature: 440, permissibleStresses: 45 },
			{ temperature: 450, permissibleStresses: 38 },
			{ temperature: 460, permissibleStresses: 33 },
			{ temperature: 470, permissibleStresses: 28 },
			{ temperature: 480, permissibleStresses: 22 },
		]
	},
]

const m_20_20K_18K_Resources: Array<IResource> = [
	{
		resource: 10000,
		property: [
			{ temperature: 100, permissibleStresses: 147 },
			{ temperature: 200, permissibleStresses: 140 },
			{ temperature: 250, permissibleStresses: 132 },
			{ temperature: 275, permissibleStresses: 126 },
			{ temperature: 300, permissibleStresses: 119 },
			{ temperature: 320, permissibleStresses: 114 },
			{ temperature: 340, permissibleStresses: 109 },
			{ temperature: 350, permissibleStresses: 106 },
			{ temperature: 360, permissibleStresses: 103 },
			{ temperature: 380, permissibleStresses: 97 },
			{ temperature: 400, permissibleStresses: 92 },
			{ temperature: 410, permissibleStresses: 89 },
			{ temperature: 420, permissibleStresses: 86 },
			{ temperature: 430, permissibleStresses: 83 },
			{ temperature: 440, permissibleStresses: 80 },
			{ temperature: 450, permissibleStresses: 77 },
			{ temperature: 460, permissibleStresses: 74 },
			{ temperature: 470, permissibleStresses: 64 },
			{ temperature: 480, permissibleStresses: 56 },
			{ temperature: 490, permissibleStresses: 49 },
			{ temperature: 500, permissibleStresses: 41 },
			{ temperature: 510, permissibleStresses: 35 },
		]
	},
	{
		resource: 100000,
		property: [
			{ temperature: 100, permissibleStresses: 147 },
			{ temperature: 200, permissibleStresses: 140 },
			{ temperature: 250, permissibleStresses: 132 },
			{ temperature: 275, permissibleStresses: 126 },
			{ temperature: 300, permissibleStresses: 119 },
			{ temperature: 320, permissibleStresses: 114 },
			{ temperature: 340, permissibleStresses: 109 },
			{ temperature: 350, permissibleStresses: 106 },
			{ temperature: 360, permissibleStresses: 103 },
			{ temperature: 380, permissibleStresses: 97 },
			{ temperature: 400, permissibleStresses: 92 },
			{ temperature: 410, permissibleStresses: 86 },
			{ temperature: 420, permissibleStresses: 79 },
			{ temperature: 430, permissibleStresses: 72 },
			{ temperature: 440, permissibleStresses: 66 },
			{ temperature: 450, permissibleStresses: 59 },
			{ temperature: 460, permissibleStresses: 52 },
			{ temperature: 470, permissibleStresses: 46 },
			{ temperature: 480, permissibleStresses: 39 },
			{ temperature: 490, permissibleStresses: 33 },
			{ temperature: 500, permissibleStresses: 26 },
		]
	},
	{
		resource: 200000,
		property: [
			{ temperature: 100, permissibleStresses: 147 },
			{ temperature: 200, permissibleStresses: 140 },
			{ temperature: 250, permissibleStresses: 132 },
			{ temperature: 275, permissibleStresses: 126 },
			{ temperature: 300, permissibleStresses: 119 },
			{ temperature: 320, permissibleStresses: 114 },
			{ temperature: 340, permissibleStresses: 109 },
			{ temperature: 350, permissibleStresses: 106 },
			{ temperature: 360, permissibleStresses: 103 },
			{ temperature: 380, permissibleStresses: 97 },
			{ temperature: 400, permissibleStresses: 78 },
			{ temperature: 410, permissibleStresses: 70 },
			{ temperature: 420, permissibleStresses: 63 },
			{ temperature: 430, permissibleStresses: 57 },
			{ temperature: 440, permissibleStresses: 50 },
			{ temperature: 450, permissibleStresses: 46 },
			{ temperature: 460, permissibleStresses: 38 },
			{ temperature: 470, permissibleStresses: 32 },
			{ temperature: 480, permissibleStresses: 27 },
		]
	},
	{
		resource: 300000,
		property: [
			{ temperature: 100, permissibleStresses: 147 },
			{ temperature: 200, permissibleStresses: 140 },
			{ temperature: 250, permissibleStresses: 132 },
			{ temperature: 275, permissibleStresses: 126 },
			{ temperature: 300, permissibleStresses: 119 },
			{ temperature: 320, permissibleStresses: 114 },
			{ temperature: 340, permissibleStresses: 109 },
			{ temperature: 350, permissibleStresses: 106 },
			{ temperature: 360, permissibleStresses: 103 },
			{ temperature: 380, permissibleStresses: 88 },
			{ temperature: 400, permissibleStresses: 71 },
			{ temperature: 410, permissibleStresses: 63 },
			{ temperature: 420, permissibleStresses: 56 },
			{ temperature: 430, permissibleStresses: 50 },
			{ temperature: 440, permissibleStresses: 44 },
			{ temperature: 450, permissibleStresses: 39 },
			{ temperature: 460, permissibleStresses: 34 },
			{ temperature: 470, permissibleStresses: 28 },
			{ temperature: 480, permissibleStresses: 24 },
		]
	},
]

const m_16GS_09G2S_Resources: Array<IResource> = [
	{
		resource: 10000,
		property: [
			{ temperature: 100, permissibleStresses: 170 },
			{ temperature: 200, permissibleStresses: 150 },
			{ temperature: 250, permissibleStresses: 145 },
			{ temperature: 275, permissibleStresses: 140 },
			{ temperature: 300, permissibleStresses: 133 },
			{ temperature: 320, permissibleStresses: 127 },
			{ temperature: 340, permissibleStresses: 122 },
			{ temperature: 350, permissibleStresses: 120 },
			{ temperature: 360, permissibleStresses: 117 },
			{ temperature: 380, permissibleStresses: 112 },
			{ temperature: 400, permissibleStresses: 107 },
			{ temperature: 410, permissibleStresses: 104 },
			{ temperature: 420, permissibleStresses: 102 },
			{ temperature: 430, permissibleStresses: 98 },
			{ temperature: 440, permissibleStresses: 95 },
			{ temperature: 450, permissibleStresses: 89 },
			{ temperature: 460, permissibleStresses: 83 },
			{ temperature: 470, permissibleStresses: 71 },
			{ temperature: 480, permissibleStresses: 60 },
		]
	},
	{
		resource: 100000,
		property: [
			{ temperature: 100, permissibleStresses: 170 },
			{ temperature: 200, permissibleStresses: 150 },
			{ temperature: 250, permissibleStresses: 145 },
			{ temperature: 275, permissibleStresses: 140 },
			{ temperature: 300, permissibleStresses: 133 },
			{ temperature: 320, permissibleStresses: 127 },
			{ temperature: 340, permissibleStresses: 122 },
			{ temperature: 350, permissibleStresses: 120 },
			{ temperature: 360, permissibleStresses: 117 },
			{ temperature: 380, permissibleStresses: 112 },
			{ temperature: 400, permissibleStresses: 107 },
			{ temperature: 410, permissibleStresses: 97 },
			{ temperature: 420, permissibleStresses: 87 },
			{ temperature: 430, permissibleStresses: 76 },
			{ temperature: 440, permissibleStresses: 68 },
			{ temperature: 450, permissibleStresses: 62 },
			{ temperature: 460, permissibleStresses: 54 },
			{ temperature: 470, permissibleStresses: 46 },
		]
	},
	{
		resource: 200000,
		property: [
			{ temperature: 100, permissibleStresses: 170 },
			{ temperature: 200, permissibleStresses: 150 },
			{ temperature: 250, permissibleStresses: 145 },
			{ temperature: 275, permissibleStresses: 140 },
			{ temperature: 300, permissibleStresses: 133 },
			{ temperature: 320, permissibleStresses: 127 },
			{ temperature: 340, permissibleStresses: 122 },
			{ temperature: 350, permissibleStresses: 120 },
			{ temperature: 360, permissibleStresses: 117 },
			{ temperature: 380, permissibleStresses: 112 },
			{ temperature: 400, permissibleStresses: 95 },
			{ temperature: 410, permissibleStresses: 83 },
			{ temperature: 420, permissibleStresses: 73 },
			{ temperature: 430, permissibleStresses: 63 },
			{ temperature: 440, permissibleStresses: 55 },
			{ temperature: 450, permissibleStresses: 46 },
			{ temperature: 460, permissibleStresses: 38 },
			{ temperature: 470, permissibleStresses: 32 },
		]
	},
]

const m_10G2S1_17GS_17G1S_17G1SU_Resources: Array<IResource> = [
	{
		resource: 10000,
		property: [
			{ temperature: 100, permissibleStresses: 177 },
			{ temperature: 200, permissibleStresses: 165 },
			{ temperature: 250, permissibleStresses: 156 },
			{ temperature: 275, permissibleStresses: 150 },
			{ temperature: 300, permissibleStresses: 144 },
			{ temperature: 320, permissibleStresses: 139 },
			{ temperature: 340, permissibleStresses: 133 },
			{ temperature: 350, permissibleStresses: 131 },
			{ temperature: 360, permissibleStresses: 127 },
			{ temperature: 380, permissibleStresses: 121 },
			{ temperature: 400, permissibleStresses: 113 },
			{ temperature: 410, permissibleStresses: 107 },
			{ temperature: 420, permissibleStresses: 102 },
			{ temperature: 430, permissibleStresses: 97 },
			{ temperature: 440, permissibleStresses: 92 },
			{ temperature: 450, permissibleStresses: 88 },
			{ temperature: 460, permissibleStresses: 82 },
			{ temperature: 470, permissibleStresses: 71 },
			{ temperature: 480, permissibleStresses: 60 },
		]
	},
	{
		resource: 100000,
		property: [
			{ temperature: 100, permissibleStresses: 177 },
			{ temperature: 200, permissibleStresses: 165 },
			{ temperature: 250, permissibleStresses: 156 },
			{ temperature: 275, permissibleStresses: 150 },
			{ temperature: 300, permissibleStresses: 144 },
			{ temperature: 320, permissibleStresses: 139 },
			{ temperature: 340, permissibleStresses: 133 },
			{ temperature: 350, permissibleStresses: 131 },
			{ temperature: 360, permissibleStresses: 127 },
			{ temperature: 380, permissibleStresses: 121 },
			{ temperature: 400, permissibleStresses: 113 },
			{ temperature: 410, permissibleStresses: 102 },
			{ temperature: 420, permissibleStresses: 90 },
			{ temperature: 430, permissibleStresses: 78 },
			{ temperature: 440, permissibleStresses: 70 },
			{ temperature: 450, permissibleStresses: 63 },
			{ temperature: 460, permissibleStresses: 54 },
			{ temperature: 470, permissibleStresses: 46 },
		]
	},
	{
		resource: 200000,
		property: [
			{ temperature: 100, permissibleStresses: 177 },
			{ temperature: 200, permissibleStresses: 165 },
			{ temperature: 250, permissibleStresses: 156 },
			{ temperature: 275, permissibleStresses: 150 },
			{ temperature: 300, permissibleStresses: 144 },
			{ temperature: 320, permissibleStresses: 139 },
			{ temperature: 340, permissibleStresses: 133 },
			{ temperature: 350, permissibleStresses: 131 },
			{ temperature: 360, permissibleStresses: 127 },
			{ temperature: 380, permissibleStresses: 121 },
			{ temperature: 400, permissibleStresses: 96 },
			{ temperature: 410, permissibleStresses: 85 },
			{ temperature: 420, permissibleStresses: 75 },
			{ temperature: 430, permissibleStresses: 65 },
			{ temperature: 440, permissibleStresses: 55 },
			{ temperature: 450, permissibleStresses: 46 },
			{ temperature: 460, permissibleStresses: 38 },
			{ temperature: 470, permissibleStresses: 32 },
		]
	},
]

const m_15GS_Resources: Array<IResource> = [
	{
		resource: 10000,
		property: [
			{ temperature: 100, permissibleStresses: 185 },
			{ temperature: 200, permissibleStresses: 169 },
			{ temperature: 250, permissibleStresses: 165 },
			{ temperature: 275, permissibleStresses: 161 },
			{ temperature: 300, permissibleStresses: 153 },
			{ temperature: 320, permissibleStresses: 145 },
			{ temperature: 340, permissibleStresses: 137 },
			{ temperature: 350, permissibleStresses: 133 },
			{ temperature: 360, permissibleStresses: 129 },
			{ temperature: 380, permissibleStresses: 121 },
			{ temperature: 400, permissibleStresses: 113 },
			{ temperature: 410, permissibleStresses: 107 },
			{ temperature: 420, permissibleStresses: 102 },
			{ temperature: 430, permissibleStresses: 97 },
			{ temperature: 440, permissibleStresses: 92 },
			{ temperature: 450, permissibleStresses: 88 },
			{ temperature: 460, permissibleStresses: 82 },
			{ temperature: 470, permissibleStresses: 71 },
			{ temperature: 480, permissibleStresses: 60 },
		]
	},
	{
		resource: 100000,
		property: [
			{ temperature: 100, permissibleStresses: 185 },
			{ temperature: 200, permissibleStresses: 169 },
			{ temperature: 250, permissibleStresses: 165 },
			{ temperature: 275, permissibleStresses: 161 },
			{ temperature: 300, permissibleStresses: 153 },
			{ temperature: 320, permissibleStresses: 145 },
			{ temperature: 340, permissibleStresses: 137 },
			{ temperature: 350, permissibleStresses: 133 },
			{ temperature: 360, permissibleStresses: 129 },
			{ temperature: 380, permissibleStresses: 121 },
			{ temperature: 400, permissibleStresses: 113 },
			{ temperature: 410, permissibleStresses: 102 },
			{ temperature: 420, permissibleStresses: 90 },
			{ temperature: 430, permissibleStresses: 78 },
			{ temperature: 440, permissibleStresses: 70 },
			{ temperature: 450, permissibleStresses: 63 },
			{ temperature: 460, permissibleStresses: 54 },
			{ temperature: 470, permissibleStresses: 46 },
		]
	},
	{
		resource: 200000,
		property: [
			{ temperature: 100, permissibleStresses: 185 },
			{ temperature: 200, permissibleStresses: 169 },
			{ temperature: 250, permissibleStresses: 165 },
			{ temperature: 275, permissibleStresses: 161 },
			{ temperature: 300, permissibleStresses: 153 },
			{ temperature: 320, permissibleStresses: 145 },
			{ temperature: 340, permissibleStresses: 137 },
			{ temperature: 350, permissibleStresses: 133 },
			{ temperature: 360, permissibleStresses: 129 },
			{ temperature: 380, permissibleStresses: 121 },
			{ temperature: 400, permissibleStresses: 96 },
			{ temperature: 410, permissibleStresses: 85 },
			{ temperature: 420, permissibleStresses: 75 },
			{ temperature: 430, permissibleStresses: 65 },
			{ temperature: 440, permissibleStresses: 55 },
			{ temperature: 450, permissibleStresses: 46 },
			{ temperature: 460, permissibleStresses: 38 },
			{ temperature: 470, permissibleStresses: 32 },
		]
	},
]

const materials: Array<IMaterialSource> = [
	{ type: typeSteel.carbon, title: 'Ст2кп', resources: m_St2kpResources },
	{ type: typeSteel.carbon, title: 'Ст3кп', resources: m_St3kpResources },
	{ type: typeSteel.carbon, title: 'Ст2сп', resources: m_St2spSt2psResources },
	{ type: typeSteel.carbon, title: 'Ст2пс', resources: m_St2spSt2psResources },
	{ type: typeSteel.carbon, title: 'Ст3сп', resources: m_St3spSt3psResources },
	{ type: typeSteel.carbon, title: 'Ст3пс', resources: m_St3spSt3psResources },
	{ type: typeSteel.carbon, title: 'Ст4сп', resources: m_St4spSt4psResources },
	{ type: typeSteel.carbon, title: 'Ст4пс', resources: m_St4spSt4psResources },
	{ type: typeSteel.carbon, title: 'Ст3Гпс', resources: m_St3GspResources },
	{ type: typeSteel.carbon, title: '22К', resources: m_St3GspResources },
	{ type: typeSteel.carbon, title: '14ГНМА', resources: m_14GNMAResources },
	{ type: typeSteel.carbon, title: '16ГНМ', resources: m_16GNM_16GNMA_Resources },
	{ type: typeSteel.carbon, title: '16ГНМА', resources: m_16GNM_16GNMA_Resources },
	{ type: typeSteel.carbon, title: '08', resources: m_08_10_12K_Resources },
	{ type: typeSteel.carbon, title: '10', resources: m_08_10_12K_Resources },
	{ type: typeSteel.carbon, title: '12К', resources: m_08_10_12K_Resources },
	{ type: typeSteel.carbon, title: '15', resources: m_15_15K_16K_Resources },
	{ type: typeSteel.carbon, title: '15K', resources: m_15_15K_16K_Resources },
	{ type: typeSteel.carbon, title: '16K', resources: m_15_15K_16K_Resources },
	{ type: typeSteel.carbon, title: '20', resources: m_20_20K_18K_Resources },
	{ type: typeSteel.carbon, title: '20K', resources: m_20_20K_18K_Resources },
	{ type: typeSteel.carbon, title: '18К', resources: m_20_20K_18K_Resources },
	{ type: typeSteel.carbon, title: '16ГС', resources: m_16GS_09G2S_Resources },
	{ type: typeSteel.carbon, title: '09Г2С', resources: m_16GS_09G2S_Resources },
	{ type: typeSteel.carbon, title: '10Г2С1', resources: m_10G2S1_17GS_17G1S_17G1SU_Resources },
	{ type: typeSteel.carbon, title: '17ГС', resources: m_10G2S1_17GS_17G1S_17G1SU_Resources },
	{ type: typeSteel.carbon, title: '17Г1С', resources: m_10G2S1_17GS_17G1S_17G1SU_Resources },
	{ type: typeSteel.carbon, title: '17Г1СУ', resources: m_10G2S1_17GS_17G1S_17G1SU_Resources },
	{ type: typeSteel.carbon, title: '15ГС', resources: m_15GS_Resources },
]

export { materials };