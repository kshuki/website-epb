import { TOKEN_KEY } from './'

export default {
    saveAuth: (userName: string, token: string) => {
        sessionStorage.setItem(TOKEN_KEY, JSON.stringify({ userName: userName, access_token: token }));
    },

    clearAuth: () => {
        sessionStorage.removeItem(TOKEN_KEY);
    },

    getLogin: () => {
        let item = sessionStorage.getItem(TOKEN_KEY);
        let login = '';
        if (item) {
            login = JSON.parse(item).userName;
        }
        return login;
    },

    isLogged: () => {
        let item = sessionStorage.getItem(TOKEN_KEY);
        if (item) {
            return true;
        } else {
            return false;
        }
    },

    getToken: () => {
        let item = sessionStorage.getItem(TOKEN_KEY);
        let token = null;
        if (item) {
            token = JSON.parse(item).access_token;
        }
        return token;
    }
}