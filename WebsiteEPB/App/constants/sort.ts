//быстрая сортировка по имени
const sortTitle = (objects: Array<{ title: string }>) => {
	const partitionTitle = (objects: Array<{ title: string }>, low: number, hi: number) => {
		const pivotPosition = Math.floor(Math.random() * objects.length);
		const pivot = objects[pivotPosition];

		while (hi >= low) {
			while (objects[hi].title > pivot.title) {
				hi--;
			}
			while (objects[low].title < pivot.title) {
				low++;
			}
			if (hi >= low) {
				const tmp = objects[low];
				objects[low] = objects[hi];
				objects[hi] = tmp;
				hi--;
				low++;
			}
		}

		return low;
	}

	const quickSortTitle = (objects: Array<{ title: string }>, low = 0, hi = objects.length - 1): Array<any> => {
		if (low < hi) {
			const index = partitionTitle(objects, low, hi);
			quickSortTitle(objects, low, index - 1);
			quickSortTitle(objects, index, hi);
		}
		return objects;
	}
	return quickSortTitle(objects);
}

export { sortTitle };