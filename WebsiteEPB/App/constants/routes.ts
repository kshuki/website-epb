export const LINKS = {
    MAIN: '/',
    LOGIN:'/login',
    REGISTER: '/register',
    PROFILE: '/user',
    EDIT_PROFILE: '/user/settings',
    CHANGE_PASSWORD: '/user/password',
    LIST_USER: '/users',
    FOLDERS: '/folders'
}