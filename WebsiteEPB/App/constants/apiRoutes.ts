const MAIN_LINK_API = window.location.origin + "/api";

//links for user
const USER_LINK_API = MAIN_LINK_API + "/user";
const FOLDER_LINK_API = MAIN_LINK_API + "/folder";
const PIPE_LINK_API = MAIN_LINK_API + "/pipe";
// export const PROFILE_LINK_API = USER_LINK_API + "/profile";
// export const LOGIN_LINK_API = USER_LINK_API + "/login";
// export const LOGOUT_LINK_API = USER_LINK_API + "/logout";


export const API = {
	GET_PROFILE: USER_LINK_API + "/profile",
	EDIT_PROFILE: USER_LINK_API + "/editprofile",
	LOGIN: USER_LINK_API + "/login",
	LOGOUT: USER_LINK_API + "/logout",
	REGISTER: USER_LINK_API + "/register",

	GET_FOLDERS: FOLDER_LINK_API + "/getfolders",
	REMOVE_FOLDER: FOLDER_LINK_API + "/removefolder",
	ADD_FOLDER: FOLDER_LINK_API + "/addfolder",
	GET_FOLDER: FOLDER_LINK_API + "/getfolder",
	CHANGE_FOLDER: FOLDER_LINK_API + "/changefolder",

	GET_PIPES: PIPE_LINK_API + "/getpipes",
	REMOVE_PIPE: PIPE_LINK_API + "/removepipe",
	ADD_PIPE: PIPE_LINK_API + "/addpipe",
	GET_PIPE: PIPE_LINK_API + "/getpipe",
	CHANGE_PIPE: PIPE_LINK_API + "/changepipe",

	ADD_PIPE_STRAIGHT: PIPE_LINK_API + "/addpipestraight",
	CHANGE_PIPE_STRAIGHT: PIPE_LINK_API + "/changepipestraight",
	REMOVE_PIPE_STRAIGHT: PIPE_LINK_API + "/removepipestraight",
}