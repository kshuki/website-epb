import React from 'react';
import { IPipe } from './../../types';


const PipelineContainer = ({ pipe }: { pipe: IPipe }) => {
	const {title, straights, bends} = pipe;
	return (
		<div>
			<h2>{title}</h2>
		</div>
	);
}

export { PipelineContainer };