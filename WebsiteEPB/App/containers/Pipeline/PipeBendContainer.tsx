import React, { useState, useEffect } from 'react';
import { IPipeBend, IPipeBendCalculation, typeSteel } from '../../types';
import { PipeBendCalculation, PermissibleStressesCalculation } from './PipeCalculation';
import { materials } from '../../constants';

const initState = (): IPipeBendCalculation => {
	const material = materials[0];
	const bend: IPipeBend = {
		pressure: 1.0,
		temperature: 20,
		outsideDiameter: 25,
		radiusBend: 150,
		ovality: 8,
		thickness: 2.5,
		wearRate: 0.1,
		weldСoefficient: 1,
		rejectionThickness: 1.0,
		material: {
			title: material.title,
			type: material.type,
			permissibleStresses: material.resources[0].property[0].permissibleStresses,
			isCustomMaterial: false,
		},
		resource: 0,
	}
	return PipeBendCalculation(bend);
}
const PipeBendContainer = () => {
	const [state, setState] = useState(initState);
	const { pressure, temperature, outsideDiameter, radiusBend, ovality, thickness, wearRate, weldСoefficient, rejectionThickness, material,
		resource, estimatedThickness, remainingServiceLife, K1, K2, K3, Y1, Y2, Y3, alpha, q, Sr, Sr1, Sr2, Sr3 } = state;

	const calculation = (bend: IPipeBend) => {
		setState(PipeBendCalculation(bend));
	}

	const changeMaterial = (id: number) => {
		if (id != materials.length) {
			let newMaterial = materials[id];
			let permissibleStresses = PermissibleStressesCalculation(newMaterial, temperature, resource);
			calculation({ ...state, material: { ...material, title: newMaterial.title, permissibleStresses, isCustomMaterial: false } });
		} else {
			setState({ ...state, material: { ...material, isCustomMaterial: true } });
		}
	}

	const changeTemperature = (temperature: number) => {
		if (material.isCustomMaterial) {
			setState({ ...state, temperature });
		} else {
			let propMaterial = materials.find(el => el.title === material.title)!;
			let permissibleStresses = PermissibleStressesCalculation(propMaterial, temperature, resource);
			calculation({ ...state, material: { ...material, permissibleStresses }, temperature });
		}
	}

	const changeResource = (resource: number) => {
		if (material.isCustomMaterial) {
			setState({ ...state, temperature });
		} else {
			let propMaterial = materials.find(el => el.title === material.title)!;
			let permissibleStresses = PermissibleStressesCalculation(propMaterial, temperature, resource);
			calculation({ ...state, material: { ...material, permissibleStresses }, resource });
		}
	}

	const onSubmit = () => {

	}

	return (
		<div>
			<form id="StraightForm" onSubmit={onSubmit}>
				<ul>
					<li>
						Давление:
						<label>
							<input
								type="number"
								placeholder="Давление"
								name="pressure"
								value={pressure}
								onChange={(event: any) => { calculation({ ...state, pressure: event.target.value }) }}
								required />
						</label>
						МПа
					</li>

					<li>
						Температура:
						<label>
							<input
								type="number"
								placeholder="Температура"
								name="temperature"
								value={temperature}
								onChange={(event: any) => changeTemperature(event.target.value)}
								required />
						</label>
						Градусы
					</li>

					<li>
						Внешний диаметр:
						<label>
							<input
								type="number"
								placeholder="Внешний диаметр"
								name="outsideDiameter"
								value={outsideDiameter}
								onChange={(event: any) => { calculation({ ...state, outsideDiameter: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Радиус кривизны оси гиба:
						<label>
							<input
								type="number"
								placeholder="Радиус кривизны оси гиба"
								name="radiusBend"
								value={radiusBend}
								onChange={(event: any) => { calculation({ ...state, radiusBend: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Овальность поперечного сечения:
						<label>
							<input
								type="number"
								placeholder="Овальность поперечного сечения"
								name="ovality"
								value={ovality}
								onChange={(event: any) => { calculation({ ...state, ovality: event.target.value }) }}
								required />
						</label>
						%
					</li>

					<li>
						Толщина измеренная:
						<label>
							<input
								type="number"
								placeholder="Толщина измеренная"
								name="thickness"
								value={thickness}
								onChange={(event: any) => { calculation({ ...state, thickness: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Скорость коррозии:
						<label>
							<input
								type="number"
								placeholder="Скорость коррозии"
								name="wearRate"
								value={wearRate}
								onChange={(event: any) => { calculation({ ...state, wearRate: event.target.value }) }}
								required />
						</label>
						мм/год
					</li>

					<li>
						Коэффициент прочности продольного сварного шва:
						<label>
							<input
								type="number"
								placeholder="Коэффициент прочности"
								name="weldСoefficient"
								value={weldСoefficient}
								onChange={(event: any) => { calculation({ ...state, weldСoefficient: event.target.value }) }}
								required />
						</label>
					</li>

					<li>
						Материал:
						{material.isCustomMaterial &&
							<>
								<label>
									<input
										type="text"
										placeholder="Материал"
										name="material"
										value={material.title}
										onChange={(event: any) => { setState({ ...state, material: { ...material, title: event.target.value } }) }}
										required />
								</label>
								<select name="materialType" onChange={(event) => calculation({ ...state, material: { ...material, type: event.target.value as typeSteel } })}>
									<option selected={material.type == typeSteel.carbon} value={typeSteel.carbon}>{typeSteel.carbon}</option>
									<option selected={material.type == typeSteel.alloy} value={typeSteel.alloy}>{typeSteel.alloy}</option>
									<option selected={material.type == typeSteel.austenitic} value={typeSteel.austenitic}>{typeSteel.austenitic}</option>
								</select >
							</>
						}
						<select name="materialList" onChange={(event) => changeMaterial(event.target.selectedIndex)}>
							{materials.map(mat => <option selected={material.isCustomMaterial && mat.title === material.title} value={mat.title}>{mat.title}</option>)}
							<option selected={material.isCustomMaterial} value="other">Другой</option>
						</select>
						{/* <button type='button' onClick={() => { }}>Выбрать из списка</button> */}
					</li>

					<li>
						Ресурс:
						<label>
							<input
								type="number"
								placeholder="Ресурс"
								name="resource"
								value={resource}
								onChange={(event: any) => changeResource(event.target.value)}
								required />
						</label>
						ч
						{/* <button>Выбрать из списка</button> */}
					</li>

					<li>
						Допускаемые напряжения:
						<label>
							<input
								type="number"
								placeholder="Допускаемое напряжение"
								name="permissibleStresses"
								value={material.permissibleStresses}
								onChange={(event: any) => { calculation({ ...state, material: { ...material, permissibleStresses: event.target.value, isCustomMaterial: true } }) }}
								required />
						</label>
						МПа
					</li>

					<li>
						Толщина отбраковочная:
						<label>
							<input
								type="number"
								placeholder="Толщина отбраковочная"
								name="rejectionThickness"
								value={rejectionThickness}
								onChange={(event: any) => { calculation({ ...state, rejectionThickness: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Коэффициенты формы:
						<ul>
							<li>α={alpha}</li>
							<li>q={q}</li>
							<li>Y1={Y1}</li>
							<li>Y2={Y2}</li>
							<li>Y3={Y3}</li>
						</ul>
					</li>

					<li>
						Торовые коэффициенты колена:
						<ul>
							<li>K1={K1}</li>
							<li>K2={K2}</li>
							<li>K3={K3}</li>
						</ul>
					</li>

					<li>
						Расчетная толщина стенки трубы: {Sr} мм
					</li>

					<li>
						Расчетная толщина стенки внешней стороны колена: {Sr1} мм
					</li>

					<li>
						Расчетная толщина стенки внутренней стороны колена: {Sr2} мм
					</li>

					<li>
						Расчетная толщина стенки нейтральной стороны колена {Sr3} мм
					</li>

					<li>
						Минимальная расчетная толщина отвода: {estimatedThickness} мм
					</li>

					<li>
						Остаточный срок службы: {remainingServiceLife} лет
					</li>

					<li>
						<button type='submit'>сохранить</button>
					</li>
				</ul>
			</form>
		</div>
	);
}

export { PipeBendContainer };