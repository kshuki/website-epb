import { IMaterialSource, IPipeStraight, IPipeBend, IPipeBendCalculation, IResource, typeSteel } from '../../types';

export const PipeStraightCalculation = (pipe: IPipeStraight): IPipeStraight => {
	const p = pipe.pressure;
	const Da = pipe.outsideDiameter;
	const fi = pipe.weldСoefficient;
	const sigma = pipe.material.permissibleStresses;
	const s = pipe.currentThickness;
	const c = pipe.rejectionThickness;
	const a = pipe.wearRate;

	const Sa = ceil(p * Da / (2 * fi * sigma + p * 1), 2);
	const Tp = Math.floor((s - Math.max(c, Sa)) / a);

	return { ...pipe, estimatedThickness: Sa, remainingServiceLife: Tp };
}

export const PermissibleStressesCalculation = (material: IMaterialSource, temperature: number, resource: number): number => {
	let propMaterial: IResource | undefined;
	let permissibleStresses: number = 0;

	if (material.resources[0].resource == 0) {
		propMaterial = material.resources[0];
	} else {
		propMaterial = material.resources.find(res => res.resource > resource);
	}

	if (propMaterial) {
		const index = propMaterial.property.findIndex(prop => prop.temperature >= temperature);

		if (index == 0) {
			permissibleStresses = propMaterial.property[0].permissibleStresses;
		} else if (index != -1) {
			//Метод интерполяции 
			let sigma1 = propMaterial.property[index - 1].permissibleStresses;
			let sigma2 = propMaterial.property[index].permissibleStresses;
			let t1 = propMaterial.property[index - 1].temperature;
			let t2 = propMaterial.property[index].temperature;
			let sigma = Math.floor((sigma1 - (sigma1 - sigma2) / (t2 - t1) * (temperature - t1)) * 10);
			let sigmaFixed = Math.floor(sigma / 10) * 10;
			permissibleStresses = (sigma - sigmaFixed > 5) ? sigmaFixed / 10 + 0.5 : sigmaFixed / 10;
		}
	}

	return permissibleStresses;
}

export const PipeBendCalculation = (bend: IPipeBend): IPipeBendCalculation => {
	const p = bend.pressure;
	const R = bend.radiusBend;
	const Da = bend.outsideDiameter;
	const fi = bend.weldСoefficient;
	const sigma = bend.material.permissibleStresses;
	const s = bend.thickness;
	const c = bend.rejectionThickness;
	const ovality = bend.ovality;
	const a = bend.wearRate;
	const T = bend.temperature;
	const typeSt = bend.material.type;


	const K1 = round((4 * R / Da + 1) / (4 * R / Da + 2), 3);
	const K2 = round((4 * R / Da - 1) / (4 * R / Da - 2), 3);
	const K3 = 1;

	const alpha = Math.max(0.03, p / (2 * sigma + p));
	const q = Math.max(2 * alpha * R / Da + 0.5, 1);

	let Y1 = 0;
	let Y2 = 0;
	let Y3 = 0;


	if (typeSt === typeSteel.carbon) {
		calculationShapeFactor(350, 400);
	}
	else if (typeSt === typeSteel.alloy) {
		calculationShapeFactor(400, 450);
	} else if (typeSteel.austenitic) {
		calculationShapeFactor(450, 500);
	}

	const Sr = ceil(p * Da / (2 * fi * sigma + p * 1), 2);
	const Sr1 = ceil(Sr * K1 * Y1, 2);
	const Sr2 = ceil(Sr * K2 * Y2, 2);
	const Sr3 = ceil(Sr * K3 * Y3, 2);

	const Sa = Math.max(Sr, Sr1, Sr2, Sr3);
	const Tp = Math.floor((s - Math.max(c, Sa)) / a);

	return { ...bend, estimatedThickness: Sa, remainingServiceLife: Tp, K1, K2, K3, Y1, Y2, Y3, alpha, q, Sr, Sr1, Sr2, Sr3 };

	function calculationShapeFactor(T1: number, T2: number) {
		if (T <= T1) {
			let values = shapeFactor_1();
			Y1 = values.Y1;
			Y3 = values.Y3;
		} else if (T >= T2) {
			let values = shapeFactor_2();
			Y1 = values.Y1;
			Y3 = values.Y3;
		} else {
			let values_1 = shapeFactor_1();
			let values_2 = shapeFactor_2();
			const interpolationСoefficient = 1 / (T2 - T1) * (T - T1);
			Y1 = values_1.Y1 - (values_1.Y1 - values_2.Y1) * interpolationСoefficient;

			Y3 = values_1.Y3 - (values_1.Y3 - values_2.Y3) * interpolationСoefficient;
		}
		Y1 = round(Y1, 3);
		Y2 = Y1;
		Y3 = round(Y3, 3);
	}

	function shapeFactor_1() {
		return {
			Y1: Math.max(1, 0.12 * (1 + Math.sqrt(1 + 0.4 * ovality / alpha * q))),
			Y3: Math.max(1, 0.12 * (1 + Math.sqrt(1 + 0.4 * ovality / alpha)))
		}
	}

	function shapeFactor_2(): { Y1: number, Y3: number } {
		return {
			Y1: Math.max(1, 0.4 * (1 + Math.sqrt(1 + 0.015 * ovality / alpha * q))),
			Y3: Math.max(1, 0.4 * (1 + Math.sqrt(1 + 0.015 * ovality / alpha)))
		}
	}
}

const ceil = (value: number, countCeil: number) => {
	let param = 1;
	for (let i = 0; i < countCeil; i++) {
		param *= 10;
	}
	return Math.ceil(value * param) / param;
}


const round = (value: number, countCeil: number) => {
	let param = 1;
	for (let i = 0; i < countCeil; i++) {
		param *= 10;
	}
	return Math.round(value * param) / param;
}