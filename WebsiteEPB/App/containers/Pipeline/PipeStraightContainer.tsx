import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { IPipeStraight } from '../../types';
import { PipeStraightCalculation, PermissibleStressesCalculation } from './PipeCalculation';
import { materials, API } from '../../constants';
import { error } from './../../reducers/error';

const initState = (idPipe: number): IPipeStraight => {
	const material = materials[0];
	const pipe: IPipeStraight = {
		id: undefined,
		idPipe: idPipe,
		pressure: 1.0,
		temperature: 20,
		outsideDiameter: 25,
		currentThickness: 2.5,
		wearRate: 0.1,
		weldСoefficient: 1,
		rejectionThickness: 1.0,
		material: {
			title: material.title,
			type: material.type,
			permissibleStresses: material.resources[0].property[0].permissibleStresses,
			isCustomMaterial: false,
		},
		resource: 0,
		estimatedThickness: 0,
		remainingServiceLife: 0,
		// currentPositionInMaterialList: materials[0].title
	}
	return PipeStraightCalculation(pipe);
}


const PipeStraightContainer = ({idParent, pipeStraight = initState(idParent!) }: {idParent?: number, pipeStraight?: IPipeStraight }) => {
	const [state, setState] = useState({
		pipeStraight: pipeStraight,
		error: undefined as undefined | string
	});

	const pipe = state.pipeStraight;
	const { pressure, temperature, outsideDiameter, currentThickness: thickness, wearRate, weldСoefficient, rejectionThickness, material, resource, estimatedThickness, remainingServiceLife } = pipe;
	
	const calculation = (pipe: IPipeStraight) => {
		setState({...state, pipeStraight: PipeStraightCalculation(pipe)});
	}

	const changeMaterial = (id: number) => {
		if (id != materials.length) {
			let newMaterial = materials[id];
			let permissibleStresses = PermissibleStressesCalculation(newMaterial, temperature, resource);
			calculation({ ...pipe, material: { ...material, title: newMaterial.title, permissibleStresses, isCustomMaterial: false } });
		} else {
			setState({ ...state, pipeStraight: {...pipe ,material: { ...material, isCustomMaterial: true }}});
		}
	}

	const changeTemperature = (temperature: number) => {
		if (material.isCustomMaterial) {
			setState({ ...state, pipeStraight: {...pipe, temperature }});
		} else {
			let propMaterial = materials.find(el => el.title === material.title)!;
			let permissibleStresses = PermissibleStressesCalculation(propMaterial, temperature, resource);
			calculation({ ...pipe, material: { ...material, permissibleStresses }, temperature });
		}
	}

	const changeResource = (resource: number) => {
		if (material.isCustomMaterial) {
			setState({ ...state, pipeStraight: {...pipe, temperature }});
		} else {
			let propMaterial = materials.find(el => el.title === material.title)!;
			let permissibleStresses = PermissibleStressesCalculation(propMaterial, temperature, resource);
			calculation({ ...pipe, material: { ...material, permissibleStresses }, resource });
		}
	}

	const setError = (error: string) => {
		setState({...state, error: error});
	}

	const savePipeStraight = (pipeStraight: IPipeStraight) => {
		// console.log(pipeStraight);
		// listPipeStraight.push(pipeStraight);
	}

	const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		createPipeStraightRequest(pipe, savePipeStraight, setError);
		console.log('test');
	}

	return (
		<div>
			<form id="StraightForm" onSubmit={onSubmit}>
				<ul>
					<li>
						Давление
						<label>
							<input
								type="number"
								placeholder="Давление"
								name="pressure"
								value={pressure}
								onChange={(event: any) => { calculation({ ...pipe, pressure: event.target.value }) }}
								required />
						</label>
						МПа
					</li>

					<li>
						Температура
						<label>
							<input
								type="number"
								placeholder="Температура"
								name="temperature"
								value={temperature}
								onChange={(event: any) => changeTemperature(event.target.value)}
								required />
						</label>
						Градусы
					</li>

					<li>
						Материал
						{material.isCustomMaterial &&
							<label>
								<input
									type="text"
									placeholder="Материал"
									name="material"
									value={material.title}
									onChange={(event: any) => { setState({ ...state, pipeStraight: {...pipe, material: {...material, title: event.target.value} }}) }}
									required />
							</label>
						}
						<select name="materialList" onChange={(event) => changeMaterial(event.target.selectedIndex)}>
							{materials.map(mat => <option selected={material.isCustomMaterial && mat.title === material.title} value={mat.title}>{mat.title}</option>)}
							<option selected={material.isCustomMaterial} value="other">Другой</option>
						</select>
						{/* <button type='button' onClick={() => { }}>Выбрать из списка</button> */}
					</li>

					<li>
						Внешний диаметр
						<label>
							<input
								type="number"
								placeholder="Внешний диаметр"
								name="outsideDiameter"
								value={outsideDiameter}
								onChange={(event: any) => { calculation({ ...pipe, outsideDiameter: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Толщина измеренная
						<label>
							<input
								type="number"
								placeholder="Толщина измеренная"
								name="thickness"
								value={thickness}
								onChange={(event: any) => { calculation({ ...pipe, currentThickness: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Скорость коррозии
						<label>
							<input
								type="number"
								placeholder="Скорость коррозии"
								name="wearRate"
								value={wearRate}
								onChange={(event: any) => { calculation({ ...pipe, wearRate: event.target.value }) }}
								required />
						</label>
						мм/год
					</li>

					<li>
						Коэффициент прочности продольного сварного шва
						<label>
							<input
								type="number"
								placeholder="Коэффициент прочности"
								name="weldСoefficient"
								value={weldСoefficient}
								onChange={(event: any) => { calculation({ ...pipe, weldСoefficient: event.target.value }) }}
								required />
						</label>
					</li>

					<li>
						Ресурс
						<label>
							<input
								type="number"
								placeholder="Ресурс"
								name="resource"
								value={resource}
								onChange={(event: any) => changeResource(event.target.value)}
								required />
						</label>
						ч
						{/* <button>Выбрать из списка</button> */}
					</li>

					<li>
						Допускаемые напряжения
						<label>
							<input
								type="number"
								placeholder="Допускаемое напряжение"
								name="permissibleStresses"
								value={material.permissibleStresses}
								onChange={(event: any) => { calculation({ ...pipe, material: { ...material, permissibleStresses: event.target.value, isCustomMaterial: true } }) }}
								required />
						</label>
						МПа
					</li>

					<li>
						Толщина отбраковочная
						<label>
							<input
								type="number"
								placeholder="Толщина отбраковочная"
								name="rejectionThickness"
								value={rejectionThickness}
								onChange={(event: any) => { calculation({ ...pipe, rejectionThickness: event.target.value }) }}
								required />
						</label>
						мм
					</li>

					<li>
						Минимальная расчетная толщина трубы
						<label>
							<input
								type="number"
								placeholder="Минимальная расчетная толщина трубы"
								name="estimatedThickness"
								value={estimatedThickness}
								required />
						</label>
						мм
					</li>

					<li>
						Остаточный срок службы
						<label>
							<input
								type="number"
								placeholder="Остаточный срок службы"
								name="remainingServiceLife"
								value={remainingServiceLife}
								required />
						</label>
						лет
					</li>

					<li>
						<button type="submit">сохранить</button>
						{/* {idParent === void(0) && <button >удалить</button>} */}
					</li>
				</ul>
			</form>
		</div>
	);
}

export { PipeStraightContainer };




const createPipeStraightRequest = (pipeStraight: IPipeStraight, setPipeStraight: (pipeStraight: IPipeStraight) => void, setError: (error: string) => void) => {
	const pipe = {
		// id: pipeStraight.id,
		idPipe: pipeStraight.idPipe,

		pressure: pipeStraight.pressure,
		temperature: pipeStraight.temperature,
		outsideDiameter: pipeStraight.outsideDiameter,

		currentThickness: pipeStraight.currentThickness,
		rejectionThickness: pipeStraight.rejectionThickness,
		estimatedThickness: pipeStraight.estimatedThickness,

		weldСoefficient: pipeStraight.weldСoefficient,
		wearRate: pipeStraight.wearRate,

		isCustomMaterial: pipeStraight.material.isCustomMaterial,
		titleMaterial: pipeStraight.material.title,
		permissibleStresses: pipeStraight.material.permissibleStresses,
		resource: pipeStraight.resource,
		
		remainingServiceLife: pipeStraight.remainingServiceLife,
	}
	
	const data = JSON.stringify(pipe);
	console.log(data);
	axios.post(
		API.ADD_PIPE_STRAIGHT,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: { data: IPipeStraight }) => {
			console.log(response.data);
			setPipeStraight(response.data);
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось создать папку, попробуйте снова");
			return;
		})
}