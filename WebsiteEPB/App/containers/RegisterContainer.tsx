import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { LINKS, API } from '../constants';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../actions/constants';
import { IProfile, IAppStore } from '../types';
import { useHistory } from 'react-router-dom';

const RegisterContainer = () => {
	const history = useHistory();

	if (useSelector((state: IAppStore) => { return state.profileModule.isLoggen })) {
		history.push(LINKS.PROFILE);
	}

	const [state, setState] = useState({
		isLoading: false,
		firstName: '',
		secondName: '',
		email: '',
		password: '',
		passwordConfirmation: '',
		accept: false
	});

	const { isLoading, firstName, secondName, email, password, passwordConfirmation, accept } = state;

	useEffect(() => {
		if (isLoading) return;
		let form: any = document.getElementById("RegisterForm");
		form.FirstName.value = firstName;
		form.SecondName.value = secondName;
		form.Email.value = email;
		form.Password.value = password;
		form.PasswordConfirmation.value = passwordConfirmation;
		form.Accept.checked = accept;
	})

	const dispatch = useDispatch();

	const onSubmit = (event: any) => {
		event.preventDefault();

		const form: any = document.getElementById("RegisterForm");
		const firstName = form.FirstName.value;
		const secondName = form.SecondName.value;
		const email = form.Email.value;
		const password = form.Password.value;
		const passwordConfirmation = form.PasswordConfirmation.value;
		const accept = form.Accept.checked;

		if (password != passwordConfirmation) {
			form.PasswordConfirmation.setCustomValidity("Пароли не совпадают");
			return;
		}

		console.log('dispatch = ', dispatch);
		setState({ ...state, isLoading: true, firstName, secondName, email, password, passwordConfirmation, accept });

		const dataRequest = {
			FirstName: firstName,
			SecondName: secondName,
			Email: email,
			Password: password
		}

		const profile: IProfile = {
			id: 0,
			firstName: firstName,
			secondName: secondName,
			isLoggen: true,
			status: ''
		}
		requestRegister(
			() => dispatch({ type: actions.LOGIN, payload: profile }), 
			dataRequest, 
			() => setState({ ...state, isLoading: false, firstName, secondName, email, password, passwordConfirmation, accept })
		);
	}

	const routeToLogin = () => {
		history.push(LINKS.LOGIN)
	}

	if (isLoading) {
		return (<div>Идет загрузка</div>)
	}
	return (
		<div className="container">
			<div className="container_form">
				<h2>Регистрация</h2>
				<form id="RegisterForm" onSubmit={onSubmit}>
					<input type="text" className="form_input_text" placeholder="Имя" name="FirstName" required />
					<input type="text" className="form_input_text" placeholder="Фамилия" name="SecondName" required />
					<input type="email" className="form_input_text" placeholder="E-mail" name="Email" required />
					<input type="password" className="form_input_text" placeholder="Пароль" name="Password" required />
					<input type="password" className="form_input_text" placeholder="Подтверждение пароля" name="PasswordConfirmation" required />
					<div className="form_checkbox">
						<label className="container_checkbox"> 
							<div>Я принимаю соглашение и бла бла бла</div>
							<input type="checkbox" name="Accept" value="true" />
							<span className="checkmark"></span>
						</label>
					</div>
					{/* <label>
						<input type="checkbox" name="Accept" value="true" required />Я принимаю соглашение и бла бла бла
					</label> */}
					<div className="form_buttons">
						<button type="submit">
							Зарегистрироваться
						</button>
						<button onClick={routeToLogin}>
						Войти
					</button>
					</div>
				</form>

			</div>
		</div>
	)
}

export { RegisterContainer };

function requestRegister(logining: (() => void), data: any, stopLoading: (() => void)) {
	axios.post(
		API.REGISTER,
		JSON.stringify(data),
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: any) => {
			logining();
			stopLoading();
			// dispatch({ type: actions.LOGIN, payload: response.data })
		})
		.catch(error => {
			if (error.response.status === 401) {
				alert('Такой e-mail уже зарегестрирован');
			} else {
				alert('Неизвестная ошибка, повторите попытку позже');
			}
			console.log("error", error);
			stopLoading();
		})
}