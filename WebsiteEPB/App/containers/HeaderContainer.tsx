import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { IAppStore, IHeader } from '../types';
import { headerLogin, LINKS, API } from '../constants'
import { actions } from '../actions/constants';
import logo from '../source/img/logo.svg';

const HeaderContainer: React.FC = () => {
	const headerModule: IHeader = useSelector((state: IAppStore) => state.headerModule);
	const dispatch = useDispatch();
	const handleLogout = useCallback(
		() => dispatch({ type: actions.LOGOUT }),
		[dispatch]
	);

	let headerBurger: any | undefined = undefined;
	let headerMenu: any;

	const checkHeaderElements = () => {
		if (headerBurger !== void (0)) return;
		headerBurger = document.getElementsByClassName('header_burger')[0];
		headerMenu = document.getElementsByClassName('header_menu')[0];
	}

	const closeMenu = () => {
		checkHeaderElements();
		headerBurger.classList.remove('active');
		headerMenu.classList.remove('active');
	}

	const openCloseMenu = () => {
		checkHeaderElements();
		if (headerBurger.classList.contains('active')) {
			closeMenu();
		} else {
			headerBurger.classList.add('active');
			headerMenu.classList.add('active');
		}
	}

	const profileButton = () => {
		if (headerModule.isLogin) {
			return (
				<ul className="header_list">
					<li>
						<Link to={LINKS.PROFILE} className="header_link" onClick={closeMenu}>{headerModule.name}</Link>
					</li>
					<li>
						<Link to="" className="header_link" onClick={() => {
							closeMenu();
							logout(handleLogout);
						}}>Выйти</Link>
					</li>
					<li>
						<Link to="/test" className="header_link" onClick={closeMenu}>test</Link>
					</li>
				</ul>
			)
		} else {
			return (
				<ul className="header_list">
					<li>
						<Link to={LINKS.LOGIN} className="header_link" onClick={closeMenu}>Войти</Link>
					</li>
					<li>
						<Link to={LINKS.REGISTER} className="header_link" onClick={closeMenu}>Регистрация</Link>
					</li>
				</ul>
			)
		}
	}

	return (
		<header className="header js_lock_padding">
			<div className="container">
				<div className="header_body">
					<Link to={LINKS.MAIN} className="header_logo" onClick={closeMenu}>
					<img src={logo} alt="Logo" />
						{/* <img src='../source/img/logo.svg' alt="Logo" /> */}
					</Link>
					<div className="header_burger" onClick={openCloseMenu}>
						<span>

						</span>
					</div>
					<nav className="header_menu">
						{profileButton()}
					</nav>
				</div>
			</div>
			<hr />
		</header>
	)
}

export default HeaderContainer;


const logout = (handleLogout: (() => void)) => {
	axios.get(API.LOGOUT, headerLogin())
		.then(response => {
			handleLogout();
			return;
		})
		.catch(error => {
			console.log('header', error)
			return;
		})
}