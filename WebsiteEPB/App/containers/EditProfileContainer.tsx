import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { IAppStore, IProfile } from '../types';
import { actions } from '../actions/constants';
import axios from 'axios';
import { API, LINKS } from '../constants';

const EditProfileContainer = () => {
	const history = useHistory();
	const profile = useSelector((state: IAppStore) => { return state.profileModule });
	if (!profile.isLoggen) {
		history.push(LINKS.LOGIN);
	}
	
	const [state, setState] = useState({
		isLoading: false,
		firstName: profile.firstName,
		secondName: profile.secondName,
	});

	const { isLoading, firstName, secondName } = state;

	useEffect(() => {
		if (isLoading) return;
		let form: any = document.getElementById("RegisterForm");
		form.FirstName.value = firstName;
		form.SecondName.value = secondName;
	})

	const dispatch = useDispatch();

	const onSubmit = (event: any) => {
		event.preventDefault();

		const form: any = document.getElementById("RegisterForm");
		const firstName = form.FirstName.value;
		const secondName = form.SecondName.value;

		setState({ ...state, isLoading: true, firstName, secondName });

		const dataRequest = {
			FirstName: firstName,
			SecondName: secondName,
		}

		const user: IProfile = {
			firstName: firstName,
			secondName: secondName,
			isLoggen: true,
			status: profile.status
		}
		requestEditProfile(() => dispatch({ type: actions.EDIT_PROFILE, payload: user }), dataRequest, () => setState({ ...state, isLoading: false }));
	}

	const backButton = () => {
		history.goBack();
	}


	if (isLoading) {
		return (<div>Идет загрузка</div>)
	}
	return (
		<div>
			<h2>Редактирование профиля</h2>
			<form id="RegisterForm" onSubmit={onSubmit}>
				<label>
					<input type="text" placeholder="Имя" name="FirstName" required />
				</label>
				<label>
					<input type="text" placeholder="Фамилия" name="SecondName" required />
				</label>
				<button type="submit">
					Сохранить изменения
				</button>
			</form>
			<button onClick={backButton}>
				Назад
			</button>
		</div>
	)
}

export { EditProfileContainer }

function requestEditProfile(editProfile: (() => void), data: any, stopLoading: (() => void)) {
	axios.patch(
		API.EDIT_PROFILE,
		JSON.stringify(data),
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: any) => {
			editProfile();
			stopLoading();
			// dispatch({ type: actions.LOGIN, payload: response.data })
		})
		.catch(error => {
			alert('Неизвестная ошибка, повторите попытку позже');
			console.log("error", error);
			stopLoading();
		})
}