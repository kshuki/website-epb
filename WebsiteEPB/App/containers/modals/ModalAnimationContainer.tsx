import React, { useEffect } from 'react';
import Portal from '../../Potral'


const ModalAnimationContainer = ({ title, closeAction, children }: { title: string, closeAction: () => void, children: React.ReactNode }) => {
	const timeout = 800;
	let container: HTMLDivElement;

	useEffect(() => {
		setTimeout(() => {
			container.classList.add('open');
		}, 0);
	}, []);

	const closeModal = () => {
		container.classList.remove('open');
		setTimeout(() => {
			closeAction();
		}, timeout);
	}

	return (
		<Portal>
			<div className="modal_container_animation" ref={modal => container = modal!}>
				<div className="modal_body">
					<div className="modal_content">
						<div className="close_button" onClick={closeModal}></div>
						<div className="modal_title">
							{title}
						</div>
						{/* <div className="modal_text"> */}
						{children}
						{/* </div> */}
					</div>
				</div>
			</div>
		</Portal>
	)
}

export default ModalAnimationContainer;

const bodyLock = () => {
	const body = document.querySelector('body')!;
	const lockPaddingElements = document.querySelectorAll('js_lock_padding') as NodeListOf<HTMLElement>; 
	const wrapper = document.querySelector('.wrapper') as HTMLDivElement;
	const lockPaddingValue = window.innerWidth - wrapper.offsetWidth + 'px';

	lockPaddingElements.forEach(element => {
		element.style.paddingRight = lockPaddingValue;
	});

	body.style.paddingRight = lockPaddingValue;
	body.classList.add('lock');
}