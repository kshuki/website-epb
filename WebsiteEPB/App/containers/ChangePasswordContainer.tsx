import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { IAppStore } from '../types';

import { LINKS } from '../constants';

const ChangePasswordContainer = () => {
	const history = useHistory();
	const profile = useSelector((state: IAppStore) => { return state.profileModule });
	if (!profile.isLoggen) {
		history.push(LINKS.LOGIN);
	}

	const [state, setState] = useState({
		isLoading: false,
		newPassword: '',
		newPasswordConfirmation: '',
		currentPassword: ''
	});

	const { isLoading, newPassword, newPasswordConfirmation, currentPassword } = state;

	useEffect(() => {
		if (isLoading) return;
		let form: any = document.getElementById("RegisterForm");
		form.NewPassword.value = newPassword;
		form.NewPasswordConfirmation.value = newPasswordConfirmation;
		form.CurrentPassword.value = currentPassword;
	})

	const dispatch = useDispatch();

	const onSubmit = (event: any) => {
		event.preventDefault();

		const form: any = document.getElementById("RegisterForm");
		const newPassword = form.NewPassword.value;
		const newPasswordConfirmation = form.NewPasswordConfirmation.value;
		const currentPassword = form.CurrentPassword.value;

		setState({ ...state, isLoading: true, newPassword, newPasswordConfirmation, currentPassword });

		const dataRequest = {
			NewPassword: newPassword,
			CurrentPassword: currentPassword,
		}

		// requestEditProfile(() => {}, dataRequest, () => setState({ ...state, isLoading: false }));
	}

	const backButton = () => {
		history.goBack();
	}


	if (isLoading) {
		return (<div>Идет загрузка</div>)
	}
	return (
		<div>
			<h2>Изменить пароль</h2>
			<form id="RegisterForm" onSubmit={onSubmit}>
				Текущий пароль:
				<label>
					<input type="text" placeholder="Текущий пароль" name="CurrentPassword" required />
				</label>
				Новый пароль:
				<label>
					<input type="text" placeholder="Новый пароль" name="NewPassword" required />
				</label>
				Подтвердите пароль
				<label>
					<input type="text" placeholder="Подтвердите пароль" name="NewPasswordConfirmation" required />
				</label>
				<button type="submit">
					Сохранить изменения
				</button>
			</form>
			<button onClick={backButton}>
				Назад
			</button>
		</div>
	)
}

export { ChangePasswordContainer }

// function requestEditProfile(editProfile: (() => void), data: any, stopLoading: (() => void)) {
// 	axios.patch(
// 		API.EDIT_PROFILE,
// 		JSON.stringify(data),
// 		{
// 			headers:
// 			{
// 				"Content-Type": "application/hal+json"
// 			}
// 		})
// 		.then((response: any) => {
// 			editProfile();
// 			stopLoading();
// 			// dispatch({ type: actions.LOGIN, payload: response.data })
// 		})
// 		.catch(error => {
// 			alert('Неизвестная ошибка, повторите попытку позже');
// 			console.log("error", error);
// 			stopLoading();
// 		})
// }