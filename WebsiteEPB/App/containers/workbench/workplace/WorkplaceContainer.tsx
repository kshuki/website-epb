import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import axios from 'axios';
import { IAppStore, IPipe, IPipeStraight, IPipeBend, IMaterialProp, typeSteel } from './../../../types';
import { ActiveContainer } from './../Actions';
import { API, materials } from '../../../constants';
import TablePipePartContainer from './TablePipePartContainer';
import { PipeStraightContainer } from '../../pipeline/PipeStraightContainer';
import AddButtonPartPipeContainer from './AddButtonPartPipeContainer';


const initState = () => {
	const state = {
		pipeId: undefined as number | undefined,
		isLoading: true as boolean,
		listPipeStraight: [] as Array<IPipeStraight>,
		ListPipeBend: [] as Array<IPipeBend>,
		error: undefined as undefined | string,
	}
	return state;
}

const WorkplaceContainer = () => {
	const pipe = useSelector((state: IAppStore) => state.workplaceModule.pipe);
	const [state, setState] = useState(initState());

	const { pipeId, isLoading, listPipeStraight, ListPipeBend, error } = state;

	const getError = (error: string) => {
		setState({ ...state, isLoading: false, error });
	}

	const getPipe = (pipeWorkplace: IPipeWorkplace) => {
		setState({
			...state,
			isLoading: false,
			error: void (0),
			ListPipeBend: pipeWorkplace.listPipeBend,
			listPipeStraight: pipeWorkplace.listPipeStraight,
		})
	}

	if (pipe !== void (0)) {
		if (pipeId !== pipe.id) {
			setState({ ...initState(), pipeId: pipe.id });
		} else if (isLoading) {
			getPipeRequest(pipeId, getPipe, getError);
		}
	}

	return (
		<div id='container_workplace' onClick={() => ActiveContainer()}>
			<div className="close_button" onClick={() => setTimeout(() => ActiveContainer(true), 0) }></div>
			{
				pipe === void (0) ?
					<>Выберите расчет</>
					:
					<>{
						isLoading ?
							<>Идет загрузка</>
							:
							<>
								{pipe.title}
								<TablePipePartContainer listPipePart={listPipeStraight}  typePart='PipeStraight'/>
								<AddButtonPartPipeContainer title='Добавить новый элемент расчета' titleModal='Расчет на прочность прямого участка трубопровода'>
									<PipeStraightContainer idParent={pipeId}/>
								</AddButtonPartPipeContainer>
							</>
					}</>
			}
		</div>
	)
}

export default WorkplaceContainer;

const getPipeRequest = (pipeId: number,
	getPipe: (pipe: IPipeWorkplace) => void,
	getError: (error: string) => void,
) => {
	console.log('getPipeRequest');
	axios.post(
		API.GET_PIPE,
		pipeId,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			getPipe(parsingPipeData(response.data));
			// console.log(response.data);
			return;
		})
		.catch(error => {
			console.log(error);
			getError("Не удалось загрузить данные о трубопроводе");
		})
}


const parsingPipeData = (data: any): IPipeWorkplace => {
	const pipe = data.pipeLine;
	const listPipeStraight: Array<IPipeStraight> = [];
	const listPipeBend: Array<IPipeBend> = [];

	const parsingMaterial = (data: any): IMaterialProp => {
		let material: IMaterialProp;
		if (data.isCustomMaterial) {
			let findMaterial = materials.find(m => m.title === data.titleMaterial);
			findMaterial = findMaterial !== void(0) ? findMaterial : materials[0];
			material = {
				title: data.titleMaterial,
				type: findMaterial.type,
				permissibleStresses: data.permissibleStresses,
				isCustomMaterial: true
			}
		} else {
			material = {
				title: data.titleMaterial,
				type: typeSteel.carbon,
				permissibleStresses: data.permissibleStresses,
				isCustomMaterial: false
			}
		}
		return material;
	}

	data.pipeStraights.forEach((pipe: any) => {
		console.log(pipe);
		const pipeStraight: IPipeStraight = { ...pipe, material: parsingMaterial(pipe),
			// id: pipe.id,
			// idPipe: pipe.idPipe,
			// pressure: pipe.pressure,
			// temperature: pipe.temperature,
			// outsideDiameter: pipe.outsideDiameter,
			// currentThickness: pipe.currentThickness,
			// rejectionThickness: pipe.rejectionThickness,
			// weldСoefficient: pipe.weldСoefficient,
			// wearRate: pipe.wearRate,
			
			// resource: pipe.resource,
			// estimatedThickness: pipe.estimatedThickness,
			// remainingServiceLife: pipe.remainingServiceLife,
		}

		listPipeStraight.push(pipeStraight);
	});

	return { pipe, listPipeStraight: listPipeStraight, listPipeBend: listPipeBend };
}

interface IPipeWorkplace {
	pipe: IPipe,
	listPipeStraight: Array<IPipeStraight>,
	listPipeBend: Array<IPipeBend>,
}