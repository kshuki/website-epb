import React from 'react';
import sortBy from 'lodash/sortBy';
import { IPipeBend, IPipeStraight } from '../../../types';
import AddButtonPartPipeContainer from './AddButtonPartPipeContainer';
import { PipeStraightContainer } from '../../pipeline/PipeStraightContainer';

const TablePipePartContainer = ({ listPipePart, typePart }: 
	{ listPipePart: Array<IPipeBend | IPipeStraight>, typePart: 'PipeStraight' | 'PipeBend'}) => 
{
	const sortList = sortBy(listPipePart, ['outsideDiameter', 'material.title', 'pressure', 'temperature', 'estimatedThickness', 'remainingServiceLife']) as Array<IPipeBend | IPipeStraight>;

	const changeCalculation = (part: IPipeBend | IPipeStraight) => {
		switch (typePart) {
			case 'PipeStraight':
				return (
					<AddButtonPartPipeContainer title='Изменить' titleModal='Расчет на прочность прямого участка трубопровода'>
						<PipeStraightContainer pipeStraight={part as IPipeStraight}/>
					</AddButtonPartPipeContainer>
				)
	
			default:
				break;
		}
	}
	return (
		<div>
			<table>
				<thead>
					<tr>
						{/* <th>ID</th> */}
						<th>Диаметр</th>
						<th>Материал</th>
						<th>Давление</th>
						<th>Температура</th>
						<th>Срок службы</th>
						<th>Действие</th>
					</tr>
				</thead>
				<tbody>
					{sortList.map((item: IPipeBend | IPipeStraight) => {
						return (
							<tr key={item.id}>
								{/* <td>{item.id}</td> */}
								<td>{item.outsideDiameter}</td>
								<td>{item.material.title}</td>
								<td>{item.pressure}</td>
								<td>{item.temperature}</td>
								<td>{item.remainingServiceLife}</td>
								<td>
									{changeCalculation(item)}
								</td>
							</tr>
						)
					})}
				</tbody>
			</table>
		</div>
	)
}


export default TablePipePartContainer;

//todo добавить сортировку