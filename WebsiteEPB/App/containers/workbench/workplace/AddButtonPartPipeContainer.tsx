import React, { useState } from 'react';
import ModalAnimationContainer from './../../modals/ModalAnimationContainer';

const AddButtonPartPipeContainer = ({ title, titleModal, children }: { title: string, titleModal:string, children: React.ReactNode }) => {
	const [isOpenModal, setOpenModal] = useState(false);

	const OpenModal = () => {
		setOpenModal(true);
	}

	const CloseModal = () => {
		setOpenModal(false);
	}
	
	return (
		<div>
			<button onClick={OpenModal}>{title}</button>
			{isOpenModal && 
				<ModalAnimationContainer title={titleModal} closeAction={CloseModal}>
					{children}
				</ModalAnimationContainer>
			}
		</div>
	)
}


export default AddButtonPartPipeContainer;