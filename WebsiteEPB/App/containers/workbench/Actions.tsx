export const ActiveContainer = (isTreeContainer = false) => {
	const workplace = document.getElementById('container_workplace');
	const tree = document.getElementById('container_tree');
	if (isTreeContainer) {
		if (workplace!.classList.contains('active_workbench')) {
			tree!.classList.add('active_workbench');
			workplace!.classList.remove('active_workbench');
		}
	} else {
		if (!workplace!.classList.contains('active_workbench')) {
			workplace!.classList.add('active_workbench');
			tree!.classList.remove('active_workbench');
		}
	}
}