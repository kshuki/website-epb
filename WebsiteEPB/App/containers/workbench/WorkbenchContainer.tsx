import React, { useState } from 'react';
import TreeContainer from './tree/TreeContainer'
import WorkplaceContainer from './workplace/WorkplaceContainer';



const nameWidthTree = 'width_tree';

if (window.localStorage.getItem(nameWidthTree) === null){
	window.localStorage.setItem(nameWidthTree, '250px');
}

const WorkbenchContainer = () => {
	let partition: HTMLDivElement | null = null;
	let tree: HTMLDivElement | null = null;
	const changeSizeWindow = (event: any) => {
		let mouseX = event.pageX;
		if (mouseX < 209) {
			mouseX = 209;
		}
		const minWidth = mouseX - 4 + 'px';
		window.localStorage.setItem(nameWidthTree, minWidth);
		// partition!.style.left = mouseX - 9 + 'px';
		// tree!.style.width = mouseX - 9 + 'px';
		tree!.style.minWidth = minWidth;
	}

	const onMouseDown = () => {
		if (tree === null) {
			tree = document.getElementById('container_tree') as HTMLDivElement;
		}
		document.addEventListener('mousemove', changeSizeWindow)
		document.addEventListener('mouseup', onMouseUp);
	}

	const onMouseUp = () => {
		document.removeEventListener('mousemove', changeSizeWindow);
		document.removeEventListener('mouseup', onMouseUp);
	}

	return (
		<div className="container_workbench">
			<TreeContainer />
			<div className='partition_workbench' ref={ref => {
					partition = ref; 
					// partition!.ondragstart = function() { return false;	}
				}} 
				onMouseDown={onMouseDown}
			>
			</div>
			<WorkplaceContainer />
		</div>
	)
}

export { WorkbenchContainer };
