import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { IAppStore, IPipe } from '../../../types';
import { API } from '../../../constants';
import { actions } from '../../../actions/constants';
import { Dispatch } from 'redux';
import { ActiveContainer } from '../Actions';


const PipeField = ({ pipe, level, updateTree }: { pipe: IPipe, updateTree: Function, level?: number }) => {
	let listFolders: NodeListOf<Element>;
	let titleBlock: HTMLDivElement | null = null;
	let isDrag = false;
	const dispatch = useDispatch();

	const localStorageNameActive = 'active_field_id';
	const isActive = window.localStorage.getItem(localStorageNameActive) === 'pipe_' + pipe.id;

	const { title, id, idParent } = pipe;

	const newLevel = level !== undefined ? level + 1 : 0;

	const dragStart = () => {
		console.log('dragstart');
		isDrag = true;
		listFolders = document.querySelectorAll('.js_folder');
		listFolders.forEach(e => {
			e.addEventListener('dragover', dragOver);
			e.addEventListener('dragenter', dragEnter);
			e.addEventListener('dragleave', dragLeave);
			e.addEventListener('drop', dragDrop);
		});

		document.addEventListener('dragend', dragEnd);
	}

	const dragEnd = () => {
		console.log('dragEnd');
		listFolders.forEach(e => {
			e.removeEventListener('dragover', dragOver);
			e.removeEventListener('dragenter', dragEnter);
			e.removeEventListener('dragleave', dragLeave);
			e.removeEventListener('drop', dragDrop);
		});

		const listHovered = document.getElementsByClassName('hovered');
		for (let item of listHovered) {
			item.classList.remove('hovered');
		}

		document.removeEventListener('dragend', dragEnd);
		onMouseUp();
	}

	const dragOver = function (event: any) {
		event.preventDefault();
		console.log('dragOver');
	}

	const dragEnter = function (this: any) {
		this.classList.add('hovered');
		console.log('dragenter');
	}

	const dragLeave = function (this: any) {
		this.classList.remove('hovered');
		console.log('dragLeave');
	}

	const dragDrop = function (this: any) {
		console.log('dragDrop');
		const parent = this.parentElement as HTMLDivElement;
		const newIdParent = Number(checkPosition(parent));

		console.log(newIdParent);
		if (newIdParent !== void (0) || newIdParent == idParent) {
			dragEnd();

			editPipeRequest(
				{ ...pipe, idParent: newIdParent },
				dispatch,
				() => {
					pipe.idParent = newIdParent;
					updateTree();
				}
			);
		}
	}

	const rightClickMouse = (event: MouseEvent) => {
		event.preventDefault();
		document.removeEventListener('click', closeContextMenu);
		const isMobileVersion = window.innerWidth > 768;
		
		dispatch({
			type: actions.OPEN_CONTEXT_MENU_PIPE, payload: {
				pipe: pipe,
				posX: isMobileVersion ? event.clientX : 0,
				posY: isMobileVersion ? event.clientY : 0,
			}
		});
		document.removeEventListener('contextmenu', rightClickMouse);

		if (isMobileVersion) {
			document.addEventListener('click', closeContextMenu);
		}
	}

	const closeContextMenu = () => {
		dispatch({ type: actions.CLOSE_CONTEXT_MENU });
		document.removeEventListener('click', closeContextMenu);
		document.removeEventListener('touchstart', closeContextMenu);
	}

	const onMouseDown = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		console.log('onMouseDown');
		switch (event.button) {
			case 0:
				document.addEventListener('dragstart', dragStart);
				document.addEventListener('mouseup', onMouseUp);
				return;
			case 2:
				document.addEventListener('contextmenu', rightClickMouse);
				return;
			default:
				return;
		}
	}

	const onMouseUp = () => {
		console.log("onMouseUp");
		const activeElement = document.getElementsByClassName('title_object active')[0];
		if (!isDrag) {
			if (activeElement !== titleBlock) {
				if (activeElement !== void (0)) {
					activeElement.classList.remove('active');
				}
				titleBlock!.classList.add('active');
				window.localStorage.setItem(localStorageNameActive, 'pipe_' + id);
			}
			setTimeout(()=>{ActiveContainer()}, 0);
			
			dispatch({
				type: actions.WORKPLACE_OPEN, payload: {
					pipe: pipe
				}
			});
		}

		isDrag = false;
		// document.removeEventListener('mouseup', onMouseUp);
		document.removeEventListener('dragstart', dragStart);
		document.removeEventListener('mouseup', onMouseUp);
	}

	const onTouchStart = (event: React.TouchEvent<HTMLDivElement>) => {
		document.addEventListener('contextmenu', rightClickMouse);
	}

	const onTouchEnd = () => {
		console.log('onTouchEnd');
		document.removeEventListener('dragstart', dragStart);
	}

	const checkPosition = (element: HTMLDivElement) => {
		let newIdParent = undefined;

		if (element.id === 'container_tree') {
			newIdParent = newIdParent === void (0) ? '0' : newIdParent;
			return newIdParent;
		}

		const splitId = element.id.split(/\s*_\s*/);
		if (splitId[0] === 'folder') {
			newIdParent = splitId[1];
		}
		return newIdParent;
	}

	return (
		<div className={'pipe_field'}
			id={'pipe_' + id}
		>
			<div className={isActive ? 'title_object active' : 'title_object'}
				draggable='true'
				ref={ref => titleBlock = ref}
				style={{ paddingLeft: (newLevel * 15) + 'px' }}
				onMouseDown={(e) => onMouseDown(e)}
				onTouchStart={onTouchStart}
				onTouchEnd={onTouchEnd}
			>
				{/* <img src={closeFolderIcon} alt="" /> {title} */}
				<span></span> {title}
				{/* <img alt="" /> {title} */}
			</div>
		</div>
	);
}

export default PipeField;


const editPipeRequest = (
	pipe: IPipe,
	dispatch: Dispatch<any>,
	movingPipe: Function) => {

	dispatch({ type: actions.LOADING_START });

	const dataFolder = {
		Id: pipe.id,
		IdParent: pipe.idParent,
		Title: pipe.title,
		IsBuy: pipe.isBuy
	}

	const data = JSON.stringify(dataFolder);
	axios.post(
		API.CHANGE_PIPE,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			movingPipe();
			dispatch({ type: actions.LOADING_STOP });
			return;
		})
		.catch(error => {
			console.log(error);
			dispatch({ type: actions.LOADING_STOP });
			dispatch({
				type: actions.ERROR_OPEN, payload: {
					message: "Не удалось переместить элемент, попробуйте снова или обновите страницу",
					reiterate: () => editPipeRequest(pipe, dispatch, movingPipe)
				}
			});
		})
}