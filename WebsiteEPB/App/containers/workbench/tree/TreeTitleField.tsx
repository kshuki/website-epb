import React from 'react';
import { useDispatch } from 'react-redux';
import { actions } from '../../../actions/constants';

const TreeTitleField = () => {
	const dispatch = useDispatch();

	const rightClickMouse = (event: MouseEvent) => {
		event.preventDefault();
		document.removeEventListener('click', closeContextMenu);
		const isPcVersion = window.innerWidth > 768;
		dispatch({
			type: actions.OPEN_CONTEXT_MENU_TITLE_TREE, payload: {
				// id: folder.id,
				posX: isPcVersion ? event.clientX : 0,
				posY: isPcVersion ? event.clientY : 0,
			}
		});
		document.removeEventListener('contextmenu', rightClickMouse);

		if (isPcVersion) {
			document.addEventListener('click', closeContextMenu);
		}
	}

	const closeContextMenu = () => {
		dispatch({ type: actions.CLOSE_CONTEXT_MENU });
		document.removeEventListener('click', closeContextMenu);
		document.removeEventListener('touchstart', closeContextMenu);
	}

	const onMouseDown = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		
		switch (event.button) {
			case 2:
				document.addEventListener('contextmenu', rightClickMouse);
				return;
			default:
				return;
		}
	}

	return (
		<div className='tree_title js_folder'
			onMouseDown={(e) => onMouseDown(e)}
		>
			<h2>Ваши папки:</h2>
		</div>
	)
}

export default TreeTitleField;