import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IAppStore } from '../../../types';
import { LINKS, API } from '../../../constants';
import { IFolder, IPipe } from '../../../types';
import FolderField from './FolderField';
import { sortTitle } from '../../../constants';
import FolderContextMenu from './contextmenu/FolderContextMenu';
import PipeContextMenu from './contextmenu/PipeContextMenu';
// import { error } from '../../../reducers/error';
// import '../../source/img/close_folder.png';
// import '../../source/img/open_folder.png';
import PipeField from './PipeField';
import { ActiveContainer } from '../Actions';
import TreeTitleField from './TreeTitleField';
import TitleContextMenu from './contextmenu/TitieContextMenu';


const TreeContainer = () => {
	const [state, setState] = useState({
		folders: [] as Array<IFolder>,
		pipes: [] as Array<IPipe>,
		isLoading: true,
		error: undefined as string | undefined,
	});
	const { isLoading, error, folders, pipes } = state;

	const history = useHistory();
	const profile = useSelector((state: IAppStore) => { return state.profileModule });

	if (!profile.isLoggen) {
		history.push(LINKS.LOGIN);
	}

	const setFolders = (folders: Array<IFolder>, pipes: Array<IPipe>) => {
		setState({ ...state, folders, pipes, isLoading: false });
	}

	const setError = (error: string) => {
		setState({ ...state, error });
	}

	const updateTree = () => {
		setState({ ...state, isLoading: true });
		setTimeout(() => {
			setState({ ...state, isLoading: false });
		}, 0);
	}

	if (isLoading && folders.length === 0) {
		getFoldersPipesRequest(setFolders, setError);
	}

	const viewForm = () => {
		if (isLoading) {
			return "Идет загрузка";
		} else {
			if (error !== void (0)) {
				return error;
			} else {
				// console.log("workbench");
				folders.forEach(folder => {
					folder.folders = [];
					folder.pipes = [];
				});

				const treeFolders = sortTitle(createTree(folders, pipes));
				// console.log(pipes);
				return (
					<>
						{treeFolders.map(folder => {
							return <FolderField folder={folder} key={'folder' + folder.id} updateTree={updateTree} />
						})}
						{pipes.map(pipe => {
							if (pipe.idParent === 0)
								return <PipeField pipe={pipe} key={'pipe' + pipe.id} updateTree={updateTree} />
						})}
					</>
				)
			}
		}
	}

	return (
		<div id="container_tree"
			className='active_workbench'
			style={{ minWidth: window.localStorage.getItem('width_tree') as string }}
			onClick={() => ActiveContainer(true)}
		>
			<TreeTitleField />
			{viewForm()}
			<FolderContextMenu updateTree={updateTree} folders={folders} pipes={pipes} />
			<PipeContextMenu updateTree={updateTree} folders={folders} pipes={pipes} />
			<TitleContextMenu updateTree={updateTree} folders={folders} pipes={pipes} />
		</div>
	)
}

export default TreeContainer;



const createTree = (folders: Array<IFolder>, pipes: Array<IPipe>) => {
	const iterTree = (folders: Array<IFolder>, idParent: number) => {
		const listFolders = folders.filter(folder => folder.idParent == idParent);
		if (listFolders === void (0)) {
			return [];
		}

		listFolders.forEach(folder => {
			folder.folders = sortTitle(iterTree(folders, folder.id));
			const filterPipes = sortTitle(pipes.filter(pipe => pipe.idParent === folder.id));
			folder.pipes = filterPipes !== void (0) ? filterPipes : [];
		});

		return listFolders;
	}

	return sortTitle(iterTree(folders, 0));
}


//запросы на сервер
async function getFoldersPipesRequest(setFoldersPipes: (folders: Array<IFolder>, pipes: Array<IPipe>) => void, setError: (error: string) => void) {
	let data = {
		pipes: undefined as any,
		folders: undefined as any,
		error: false
	}
	await Promise.all([
		axios.post(
			API.GET_FOLDERS,
			{},
			{
				headers:
				{
					"Content-Type": "application/hal+json"
				}
			})
			.then((response: { data: Array<IFolder> }) => {
				data.folders = response.data;
			})
			.catch(error => {
				data.error = true;
				console.log("error:", error);
			}),
		axios.post(
			API.GET_PIPES,
			{},
			{
				headers:
				{
					"Content-Type": "application/hal+json"
				}
			})
			.then((response: { data: Array<IPipe> }) => {
				data.pipes = response.data;
			})
			.catch(error => {
				data.error = true;
				console.log("error:", error);
			}),
	]);

	if (data.error) {
		setError('Неизвестная ошибка, повторите попытку позже');
	} else {
		setFoldersPipes(data.folders, data.pipes);
	}

}
