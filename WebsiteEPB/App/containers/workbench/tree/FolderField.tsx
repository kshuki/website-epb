import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { IFolder, IAppStore } from '../../../types';
import { sortTitle } from '../../../constants';
import { API } from '../../../constants';
import { actions } from '../../../actions/constants';
import { Dispatch } from 'redux';
import PipeField from './PipeField';
// import openFolderIcon from './open_folder.png';
// import closeFolderIcon from './close_folder.png';
// import { actionsTree } from '../../actions/constants';


const FolderField = ({ folder, level, updateTree }: { folder: IFolder, updateTree: Function, level?: number }) => {
	let currentFolder: HTMLDivElement | null = null;
	// let children: HTMLDivElement | null = null;
	let listFolders: NodeListOf<Element>;
	let titleBlock: HTMLDivElement | null = null;
	let isDrag = false;
	const dispatch = useDispatch();
	const idProfile: number = useSelector((state: IAppStore) => state.profileModule.id);

	const localStorageNameOpen = 'open_folder_' + folder.id;
	const localStorageNameActive = 'active_field_id';

	if (window.localStorage.getItem(localStorageNameOpen) === null) {
		window.localStorage.setItem(localStorageNameOpen, "false");
	}

	const isOpen = window.localStorage.getItem(localStorageNameOpen) == 'true';
	const isActive = window.localStorage.getItem(localStorageNameActive) === 'folder_' + folder.id;

	const { title, folders, pipes, id, idParent } = folder;

	const newLevel = level !== undefined ? level + 1 : 0;

	folder.folders = sortTitle(folder.folders);
	folder.pipes = sortTitle(folder.pipes);

	const dragStart = () => {
		console.log('dragstart');
		isDrag = true;
		listFolders = document.querySelectorAll('.js_folder');
		listFolders.forEach(e => {
			e.addEventListener('dragover', dragOver);
			e.addEventListener('dragenter', dragEnter);
			e.addEventListener('dragleave', dragLeave);
			e.addEventListener('drop', dragDrop);
		});

		document.addEventListener('dragend', dragEnd);
	}

	const dragEnd = () => {
		console.log('dragEnd');
		listFolders.forEach(e => {
			e.removeEventListener('dragover', dragOver);
			e.removeEventListener('dragenter', dragEnter);
			e.removeEventListener('dragleave', dragLeave);
			e.removeEventListener('drop', dragDrop);
		});

		const listHovered = document.getElementsByClassName('hovered');
		for (let item of listHovered) {
			item.classList.remove('hovered');
		}

		document.removeEventListener('dragend', dragEnd);
		onMouseUp();
	}

	const dragOver = function (event: any) {
		event.preventDefault();
		console.log('dragOver');
	}

	const dragEnter = function (this: any) {
		this.classList.add('hovered');
		console.log('dragenter');
	}

	const dragLeave = function (this: any) {
		this.classList.remove('hovered');
		console.log('dragLeave');
	}

	const dragDrop = function (this: any) {
		console.log('dragDrop');
		const parent = this.parentElement as HTMLDivElement;
		const newIdParent = Number(checkPosition(parent));

		if (newIdParent !== void (0) || newIdParent == idParent) {
			dragEnd();

			editFolderRequest(
				{ ...folder, idParent: newIdParent },
				idProfile,
				dispatch,
				() => {
					folder.idParent = newIdParent;
					updateTree();
				}
			);
		}
	}

	const rightClickMouse = (event: MouseEvent) => {
		event.preventDefault();
		document.removeEventListener('click', closeContextMenu);
		const isMobileVersion = window.innerWidth > 768;
		dispatch({
			type: actions.OPEN_CONTEXT_MENU_FOLDER, payload: {
				// id: folder.id,
				folder: folder,
				posX: isMobileVersion ? event.clientX : 0,
				posY: isMobileVersion ? event.clientY : 0,
			}
		});
		document.removeEventListener('contextmenu', rightClickMouse);

		if (isMobileVersion) {
			document.addEventListener('click', closeContextMenu);
		}
	}

	const closeContextMenu = () => {
		dispatch({ type: actions.CLOSE_CONTEXT_MENU });
		document.removeEventListener('click', closeContextMenu);
		document.removeEventListener('touchstart', closeContextMenu);
	}

	const onMouseDown = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		console.log('onMouseDown');
		switch (event.button) {
			case 0:
				document.addEventListener('dragstart', dragStart);
				document.addEventListener('mouseup', onMouseUp);
				return;
			case 2:
				document.addEventListener('contextmenu', rightClickMouse);
				return;
			default:
				return;
		}
	}

	const onMouseUp = () => {
		console.log("onMouseUp");
		const activeElement = document.getElementsByClassName('title_object active')[0];
		if (!isDrag) {
			if (activeElement !== titleBlock) {
				if (activeElement !== void (0)) {
					activeElement.classList.remove('active');
				}
				titleBlock!.classList.add('active');
				window.localStorage.setItem(localStorageNameActive, 'folder_' + id);
			}

			if (currentFolder!.classList.contains('close')) {
				currentFolder!.classList.remove('close');
				window.localStorage.setItem(localStorageNameOpen, 'true');
			} else {
				currentFolder!.classList.add('close');
				window.localStorage.setItem(localStorageNameOpen, 'false');
			}
		}

		isDrag = false;
		// document.removeEventListener('mouseup', onMouseUp);
		document.removeEventListener('dragstart', dragStart);
		document.removeEventListener('mouseup', onMouseUp);
	}

	const onTouchStart = (event: React.TouchEvent<HTMLDivElement>) => {
		document.addEventListener('contextmenu', rightClickMouse);
		// document.addEventListener('dragstart', dragStart);
	}

	const onTouchEnd = () => {
		console.log('onTouchEnd');
		document.removeEventListener('dragstart', dragStart);
	}

	const checkPosition = (element: HTMLDivElement) => {
		let newIdParent = undefined;

		const iterPosition = (HTMLElement: HTMLDivElement, newIdParent: string | undefined): string | undefined => {
			if (HTMLElement.id === 'container_tree') {
				newIdParent = newIdParent === void (0) ? '0' : newIdParent;
				return newIdParent;
			}

			const splitId = HTMLElement.id.split(/\s*_\s*/);
			if (splitId[0] === 'folder') {
				if (splitId[1] === id.toString()) {
					return undefined;
				} else {
					if (newIdParent === void (0)) {
						newIdParent = splitId[1];
					}
				}
			}

			return iterPosition(HTMLElement.parentElement! as HTMLDivElement, newIdParent);
		}

		return iterPosition(element, newIdParent);
	}

	return (
		<div className={isOpen ? 'folder_field' : 'folder_field close'}
			ref={ref => currentFolder = ref}
			id={'folder_' + id}
		>
			<div className={isActive ? 'title_object js_folder active' : 'title_object js_folder'}
				draggable='true'
				ref={ref => titleBlock = ref}
				style={{ paddingLeft: (newLevel * 15) + 'px' }}
				onMouseDown={(e) => onMouseDown(e)}
				onTouchStart={onTouchStart}
				onTouchEnd={onTouchEnd}
			>
				{/* <img src={closeFolderIcon} alt="" /> {title} */}
				<span></span> {title}
				{/* <img alt="" /> {title} */}
			</div>
			<div className='children'>
				{folders.map(folder => {
					return <FolderField folder={folder} level={newLevel} key={id} updateTree={updateTree} />
				})}
				{pipes.map(pipe => {
					return <PipeField pipe={pipe} key={'pipe' + pipe.id} updateTree={updateTree} level={newLevel}/>
				})}
			</div>
		</div>
	);
}

export default FolderField;


const editFolderRequest = (
	folder: IFolder,
	idProfile: number,
	dispatch: Dispatch<any>,
	movingFolder: Function) => {

	dispatch({ type: actions.LOADING_START });

	const dataFolder = {
		Id: folder.id,
		IdParent: folder.idParent,
		Title: folder.title,
		IdProfile: idProfile,
	}

	const data = JSON.stringify(dataFolder);
	axios.post(
		API.CHANGE_FOLDER,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			movingFolder();
			dispatch({ type: actions.LOADING_STOP });
			return;
		})
		.catch(error => {
			console.log(error);
			dispatch({ type: actions.LOADING_STOP });
			dispatch({
				type: actions.ERROR_OPEN, payload: {
					message: "Не удалось переместить папку, попробуйте снова или обновите страницу",
					reiterate: () => editFolderRequest(folder, idProfile, dispatch, movingFolder)
				}
			});
		})
}