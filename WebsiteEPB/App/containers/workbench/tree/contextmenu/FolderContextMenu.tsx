import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { IContextMenu, IAppStore, typeContextMenu, IFolder, IPipe } from '../../../../types';
import { API } from '../../../../constants';
import NameInputModal from './NameInputModal'
import DeleteFieldModal from './DeleteFieldModal';
import MoveFolderModal from './MoveFolderModal';
import { actions } from '../../../../actions/constants';

enum folderContextMenuState {
	createFolder = "createFolder",
	createPipe = "createPipe",
	rename = "rename",
	remove = "remove",
	move = "move",
	close = "close"
}

const FolderContextMenu = ({ updateTree, folders, pipes }: { updateTree: Function, folders: Array<IFolder>, pipes: Array<IPipe> }) => {
	const menu: IContextMenu = useSelector((state: IAppStore) => state.contextMenuModule);

	const [state, setState] = useState(folderContextMenuState.close);

	const dispatch = useDispatch();
	const closeContextMenu = () => {
		dispatch({ type: actions.CLOSE_CONTEXT_MENU });
		document.removeEventListener('click', closeContextMenu);
	}

	const createFolderButton = () => {
		closeContextMenu();
		setState(folderContextMenuState.createFolder);
	}
	const createCalculationButton = () => {
		closeContextMenu();
		setState(folderContextMenuState.createPipe);
	}
	const renameButton = () => {
		closeContextMenu();
		setState(folderContextMenuState.rename);
	}
	const moveButton = () => {
		closeContextMenu();
		setState(folderContextMenuState.move);
	}
	const removeButton = () => {
		closeContextMenu();
		setState(folderContextMenuState.remove);
	}

	const setCloseState = () => {
		closeContextMenu();
		setState(folderContextMenuState.close);
	}



	//операции с деревом
	const addFolder = (folder: IFolder) => {
		folders.push(folder);
		updateTree();
		setState(folderContextMenuState.close);
	}

	const addPipe = (pipe: IPipe) => {
		pipes.push(pipe);
		updateTree();
		setState(folderContextMenuState.close);
	}

	const renameFolder = (title: string) => {
		menu.folder!.title = title;
		updateTree();
		setState(folderContextMenuState.close);
	}

	const moveFolder = (idParent: number) => {
		menu.folder!.idParent = idParent;
		updateTree();
		setState(folderContextMenuState.close);
	}

	const removeFolder = () => {
		folders.forEach((f, index) => {
			if (f.idParent == menu.folder!.id) {
				folders.splice(index, 1);
			}
		});

		pipes.forEach((pipe, index) => {
			if (pipe.idParent == menu.folder!.id) {
				folders.splice(index, 1);
			}
		});

		let index = folders.indexOf(menu.folder!);
		folders.splice(index, 1);
		updateTree();
		setState(folderContextMenuState.close);
	}


	if (menu.type !== typeContextMenu.folder) {
		switch (state) {
			case folderContextMenuState.createFolder:
				const actionCreateFolder = (titleObject: string, errorFunction: (text: string) => void) => {
					createFolderRequest({ idParent: menu.folder!.id, title: titleObject }, addFolder, errorFunction)
				}
				return (<NameInputModal title="Создать папку" action={actionCreateFolder} cancelFunction={setCloseState} />);

			case folderContextMenuState.createPipe:
				const actionCreatePipe = (titleObject: string, errorFunction: (text: string) => void) => {
					createPipeRequest({ idParent: menu.folder!.id, title: titleObject }, addPipe, errorFunction)
				}
				return (<NameInputModal title="Создать расчет трубопровода" action={actionCreatePipe} cancelFunction={setCloseState} />);

			case folderContextMenuState.rename:
				const actionRename = (titleObject: string, errorFunction: (text: string) => void) => {
					const dataFolder = {
						Id: menu.folder!.id,
						IdParent: menu.folder!.idParent,
						Title: titleObject,
					}
					editFolderRequest(dataFolder, () => renameFolder(titleObject), errorFunction);
				}
				return (<NameInputModal title="Переименовать папку" action={actionRename} cancelFunction={setCloseState} titleObject={menu.folder!.title} />);

			case folderContextMenuState.move:
				const actionMove = (idParent: number, errorFunction: (text: string) => void) => {
					console.log(idParent);
					const dataFolder = {
						Id: menu.folder!.id,
						IdParent: idParent,
						Title: menu.folder!.title,
					}
					editFolderRequest(dataFolder, () => moveFolder(idParent), errorFunction);
				}
				return (<MoveFolderModal action={actionMove} cancelFunction={setCloseState} listFolder={folders} currentFolder={menu.folder!} />);

			case folderContextMenuState.remove:
				const actionRemove = (errorFunction: (text: string) => void) => {
					deleteFolderRequest(menu.folder!.id, removeFolder, errorFunction)
				}
				return (<DeleteFieldModal title="Удалить папку" action={actionRemove} cancelFunction={setCloseState} />)
			default:
				return (<></>)
		}
		// return (
		// 	<>
		// 		{state.isDeleteFolder && <></>}
		// 		{state.isEnterName && <NameInputModal/>}
		// 	</>
		// )
	}

	return (
		<div className="right_click_menu" style={{ top: `${menu.posY}px`, left: `${menu.posX}px` }}>
			<div className="close_button" onClick={closeContextMenu} >
			</div>
			<ul className="right_click_list">
				<li onClick={createFolderButton}>Создать папку</li>
				<li onClick={createCalculationButton}>Создать расчет трубопровода</li>
				<li onClick={renameButton}>Переименовать</li>
				<li onClick={moveButton}>Переместить</li>
				<li onClick={removeButton}>Удалить</li>
			</ul>
		</div>
	)
}

export default FolderContextMenu;



const createFolderRequest = (folder: { idParent: number, title: string }, setFolder: (folder: IFolder) => void, setError: (error: string) => void) => {
	const data = JSON.stringify(folder);
	axios.post(
		API.ADD_FOLDER,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: { data: IFolder }) => {
			setFolder(response.data);
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось создать папку, попробуйте снова");
			return;
		})
}


const createPipeRequest = (pipe: { idParent: number, title: string }, setFolder: (folder: IPipe) => void, setError: (error: string) => void) => {
	const data = JSON.stringify(pipe);
	axios.post(
		API.ADD_PIPE,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: { data: IPipe }) => {
			setFolder(response.data);
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось создать папку, попробуйте снова");
			return;
		})
}

const editFolderRequest = (
	dataFolder: { Id: number, IdParent: number, Title: string },
	actionEdit: () => void,
	setError: (error: string) => void) => {

	const data = JSON.stringify(dataFolder);
	axios.post(
		API.CHANGE_FOLDER,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			actionEdit();
			return;
		})
		.catch(error => {
			console.log(error);
			setError("Не удалось переименовать папку, попробуйте снова");
		})
}


const deleteFolderRequest = (id: number, removeFolder: Function, setError: (error: string) => void) => {
	const data = JSON.stringify({ id: id });
	axios.post(
		API.REMOVE_FOLDER,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			removeFolder();
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось удалить папку, попробуйте снова");
			return;
		})
}