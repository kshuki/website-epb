import React, { useState, useEffect } from 'react';
import PulseLoader from "react-spinners/PulseLoader";

const NameInputModal = ({ title, action, cancelFunction, titleObject }:
	{
		title: string,
		action: (titleObject: string, errorFunction: (text: string) => void) => void,
		cancelFunction: () => void,
		titleObject?: string
	}) => {

	const [state, setState] = useState({
		isLoading: false,
		fieldTitle: titleObject != void(0) ? titleObject : "",
		error: ''
	});

	const { isLoading, fieldTitle, error } = state;

	useEffect(() => {
		if (isLoading) return;

		let inputTitle: HTMLInputElement = document.getElementById("form_input_text") as HTMLInputElement;
		inputTitle.value = fieldTitle;
	})

	const setErrorState = (error: string, fieldTitle: string) => {
		setState({ ...state, error, isLoading: false, fieldTitle });
	}

	const okButton = () => {
		let inputTitle: HTMLInputElement = document.getElementById("form_input_text") as HTMLInputElement;
		const fieldTitle = inputTitle.value;
		setState({ ...state, isLoading: true, error: '', fieldTitle })
		action(fieldTitle, (error: string) => setErrorState(error, fieldTitle));
	}

	const showError = () => {
		if (error === "") return;
		return (<div className="error_message">{error}</div>);
	}

	return (
		<div className="modal_container">
			<div className="modal_window">
				<div className="modal_text">
					<h2>{title}</h2>
					{/* {errorModule.message} */}
				</div>
				{
					isLoading
						?
						<div className="sweet_loading" style={{ marginLeft: 'auto', marginRight: 'auto' }}>
							<PulseLoader
								// css={override}
								// size={150}
								color={"#18b5a4"}
								loading={true}
							/>
						</div>
						:
						<>
						{/* <form id="enter_name_form"> */}
							<input type="text" className="form_input_text" id="form_input_text"/>
							{showError()}
							<div className="modal_buttons form" style={{ justifyContent: 'space-between' }}>
								<button onClick={okButton}>Ок</button>
								<button onClick={cancelFunction}>Отмена</button>
							</div>
						{/* </form> */}
						</>
				}
			</div>
		</div>
	)
}

export default NameInputModal;