import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { IContextMenu, IAppStore, typeContextMenu, IFolder, IPipe } from '../../../../types';
import { API } from '../../../../constants';
import NameInputModal from './NameInputModal'
import DeleteFieldModal from './DeleteFieldModal';
import MoveCalculationModal from './MoveCalculationModal';
import { actions } from '../../../../actions/constants';

enum pipeContextMenuState {
	rename = "rename",
	remove = "remove",
	move = "move",
	close = "close"
}

const PipeContextMenu = ({ updateTree, folders, pipes }: { updateTree: Function, folders: Array<IFolder>, pipes: Array<IPipe> }) => {
	const menu: IContextMenu = useSelector((state: IAppStore) => state.contextMenuModule);

	const [state, setState] = useState(pipeContextMenuState.close);

	const dispatch = useDispatch();
	const closeContextMenu = () => {
		dispatch({ type: actions.CLOSE_CONTEXT_MENU });
		document.removeEventListener('click', closeContextMenu);
	}

	const renameButton = () => {
		closeContextMenu();
		setState(pipeContextMenuState.rename);
	}
	const moveButton = () => {
		closeContextMenu();
		setState(pipeContextMenuState.move);
	}
	const removeButton = () => {
		closeContextMenu();
		setState(pipeContextMenuState.remove);
	}

	const setCloseState = () => {
		closeContextMenu();
		setState(pipeContextMenuState.close);
	}



	//операции с деревом
	const renamePipe = (title: string) => {
		menu.pipe!.title = title;
		dispatch({
			type: actions.WORKPLACE_UPDATE, payload: {
				pipe: menu.pipe!
			}
		});
		updateTree();
		setState(pipeContextMenuState.close);
	}

	const movePipe = (idParent: number) => {
		menu.pipe!.idParent = idParent;
		dispatch({
			type: actions.WORKPLACE_UPDATE, payload: {
				pipe: menu.pipe!
			}
		});
		updateTree();
		setState(pipeContextMenuState.close);
	}

	const removePipe = () => {
		console.log('removePipe');
		dispatch({
			type: actions.WORKPLACE_CLOSE, payload: {
				pipe: menu.pipe!
			}
		});
		let index = pipes.indexOf(menu.pipe!);
		pipes.splice(index, 1);
		updateTree();
		setState(pipeContextMenuState.close);
	}


	if (menu.type !== typeContextMenu.pipe) {
		switch (state) {
			case pipeContextMenuState.rename:
				const actionRename = (titleObject: string, errorFunction: (text: string) => void) => {
					const dataPipe = {
						Id: menu.pipe!.id,
						IdParent: menu.pipe!.idParent,
						Title: titleObject,
						IsBue: menu.pipe!.isBuy,
					}
					editPipeRequest(dataPipe, () => renamePipe(titleObject), errorFunction);
				}
				return (<NameInputModal title="Переименовать название трубопровода" action={actionRename} cancelFunction={setCloseState} titleObject={menu.pipe!.title} />);

			case pipeContextMenuState.move:
				const actionMove = (idParent: number, errorFunction: (text: string) => void) => {
					console.log(idParent);
					const dataPipe = {
						Id: menu.pipe!.id,
						IdParent: idParent,
						Title: menu.pipe!.title,
						IsBue: menu.pipe!.isBuy,
					}
					editPipeRequest(dataPipe, () => movePipe(idParent), errorFunction);
				}
				return (<MoveCalculationModal title='Переместить расчет трубопровода' action={actionMove} cancelFunction={setCloseState} listFolder={folders} idElement={menu.pipe!.id} />);

			case pipeContextMenuState.remove:
				const actionRemove = (errorFunction: (text: string) => void) => {
					deletePipeRequest(menu.pipe!.id, removePipe, errorFunction)
				}
				return (<DeleteFieldModal title="Удалить расчет трубопровода" action={actionRemove} cancelFunction={setCloseState} />)
			default:
				return (<></>)
		}
		// return (
		// 	<>
		// 		{state.isDeleteFolder && <></>}
		// 		{state.isEnterName && <NameInputModal/>}
		// 	</>
		// )
	}

	return (
		<div className="right_click_menu" style={{ top: `${menu.posY}px`, left: `${menu.posX}px` }}>
			<div className="close_button" onClick={closeContextMenu} >
			</div>
			<ul className="right_click_list">
				<li onClick={renameButton}>Переименовать</li>
				<li onClick={moveButton}>Переместить</li>
				<li onClick={removeButton}>Удалить</li>
			</ul>
		</div>
	)
}

export default PipeContextMenu;


const editPipeRequest = (
	dataPipe: { Id: number, IdParent: number, Title: string, IsBue: boolean },
	actionEdit: () => void,
	setError: (error: string) => void) => {

	const data = JSON.stringify(dataPipe);
	axios.post(
		API.CHANGE_PIPE,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			// renameFolder(newTitle);
			actionEdit();
			return;
		})
		.catch(error => {
			console.log(error);
			setError("Не удалось переименовать папку, попробуйте снова");
		})
}


const deletePipeRequest = (id: number, removePipe: Function, setError: (error: string) => void) => {
	const data = JSON.stringify({ id: id });
	axios.post(
		API.REMOVE_PIPE,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then(response => {
			removePipe();
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось удалить расчет трубопровода, попробуйте снова");
			return;
		})
}