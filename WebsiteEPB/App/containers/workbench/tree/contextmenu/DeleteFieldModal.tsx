import React, { useState } from 'react';
import PulseLoader from "react-spinners/PulseLoader";

const DeleteFieldModal = ({ title, action, cancelFunction }:
	{
		title: string,
		action: (errorFunction: (text: string) => void) => void,
		cancelFunction: () => void
	}) => {

	const [state, setState] = useState({
		isLoading: false,
		error: ''
	});

	const { isLoading, error } = state;

	const setErrorState = (error: string) => {
		setState({ ...state, error, isLoading: false });
	}

	const removeButton = () => {
		action(setErrorState);
	}

	const showError = () => {
		if (error === "") return;
		return (<div className="error_message">{error}</div>);
	}

	return (
		<div className="modal_container">
			<div className="modal_window">
				<div className="modal_text">
					<h2>{title}</h2>
					{/* {errorModule.message} */}
				</div>
				{
					isLoading
						?
						<div className="sweet_loading" style={{ marginLeft: 'auto', marginRight: 'auto' }}>
							<PulseLoader
								// css={override}
								// size={150}
								color={"#18b5a4"}
								loading={true}
							/>
						</div>
						:
						<>
							{showError()}
							<div className="modal_buttons form" style={{ justifyContent: 'space-between' }}>
								<button onClick={removeButton}>Удалить</button>
								<button onClick={cancelFunction}>Отмена</button>
							</div>
						</>
				}
			</div>
		</div>
	)
}

export default DeleteFieldModal;