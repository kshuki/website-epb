import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { IContextMenu, IAppStore, typeContextMenu, IFolder, IPipe } from '../../../../types';
import { API } from '../../../../constants';
import NameInputModal from './NameInputModal'
import { actions } from '../../../../actions/constants';

enum titleContextMenuState {
	createFolder = "createFolder",
	createPipe = "createPipe",
	close = "close"
}

const TitleContextMenu = ({ updateTree, folders, pipes }: { updateTree: Function, folders: Array<IFolder>, pipes: Array<IPipe> }) => {
	const menu: IContextMenu = useSelector((state: IAppStore) => state.contextMenuModule);

	const [state, setState] = useState(titleContextMenuState.close);

	const dispatch = useDispatch();
	const closeContextMenu = () => {
		dispatch({ type: actions.CLOSE_CONTEXT_MENU });
		document.removeEventListener('click', closeContextMenu);
	}

	const createFolderButton = () => {
		closeContextMenu();
		setState(titleContextMenuState.createFolder);
	}
	const createCalculationButton = () => {
		closeContextMenu();
		setState(titleContextMenuState.createPipe);
	}
	const setCloseState = () => {
		closeContextMenu();
		setState(titleContextMenuState.close);
	}


	//операции с деревом
	const addFolder = (folder: IFolder) => {
		folders.push(folder);
		updateTree();
		setState(titleContextMenuState.close);
	}
	const addPipe = (pipe: IPipe) => {
		pipes.push(pipe);
		updateTree();
		setState(titleContextMenuState.close);
	}


	if (menu.type !== typeContextMenu.title) {
		switch (state) {
			case titleContextMenuState.createFolder:
				const actionCreateFolder = (titleObject: string, errorFunction: (text: string) => void) => {
					createFolderRequest({ idParent: 0, title: titleObject }, addFolder, errorFunction)
				}
				return (<NameInputModal title="Создать папку" action={actionCreateFolder} cancelFunction={setCloseState} />);

			case titleContextMenuState.createPipe:
				const actionCreatePipe = (titleObject: string, errorFunction: (text: string) => void) => {
					createPipeRequest({ idParent: 0, title: titleObject }, addPipe, errorFunction)
				}
				return (<NameInputModal title="Создать расчет трубопровода" action={actionCreatePipe} cancelFunction={setCloseState} />);

			default:
				return (<></>)
		}
	}

	return (
		<div className="right_click_menu" style={{ top: `${menu.posY}px`, left: `${menu.posX}px` }}>
			<div className="close_button" onClick={closeContextMenu} >
			</div>
			<ul className="right_click_list">
				<li onClick={createFolderButton}>Создать папку</li>
				<li onClick={createCalculationButton}>Создать расчет трубопровода</li>
			</ul>
		</div>
	)
}

export default TitleContextMenu;



const createFolderRequest = (folder: { idParent: number, title: string }, setFolder: (folder: IFolder) => void, setError: (error: string) => void) => {
	const data = JSON.stringify(folder);
	axios.post(
		API.ADD_FOLDER,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: { data: IFolder }) => {
			setFolder(response.data);
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось создать папку, попробуйте снова");
			return;
		})
}


const createPipeRequest = (pipe: { idParent: number, title: string }, setFolder: (folder: IPipe) => void, setError: (error: string) => void) => {
	const data = JSON.stringify(pipe);
	axios.post(
		API.ADD_PIPE,
		data,
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: { data: IPipe }) => {
			setFolder(response.data);
			return;
		})
		.catch(error => {
			console.log("error", error);
			setError("Не удалось создать папку, попробуйте снова");
			return;
		})
}