import React, { useState, useEffect } from 'react';
import PulseLoader from "react-spinners/PulseLoader";
import { IFolder } from '../../../../types';


const MoveFolderModal = ({ action, cancelFunction, listFolder, currentFolder }: {
	action: (idParent: number, errorFunction: (text: string) => void) => void,
	cancelFunction: () => void,
	listFolder: Array<IFolder>,
	currentFolder: IFolder
}) => {
	const [state, setState] = useState({
		idParent: "",
		isLoading: false,
		error: ''
	});

	let select: HTMLSelectElement
	useEffect(() => {
		const blackList = filterFolders(listFolder, currentFolder);
		listFolder.forEach(folder => {
			const inBlackList = blackList.indexOf(folder.id) !== -1;
			if (!inBlackList) {
				let option = document.createElement("option");
				option.text = folder.title;
				option.value = folder.id.toString();
				select.add(option);
			}
		});
	}, [currentFolder]);

	const { isLoading, error } = state;

	const setErrorState = (error: string) => {
		setState({ ...state, error, isLoading: false });
	}

	const okButton = () => {
		switch (select.value) {
			case "":
				setState({ ...state, error: "Выберите папку" });
				break;
			case currentFolder.id.toString():
				cancelFunction();
				break;
			default:
				setState({ ...state, isLoading: true, error: '', idParent: select.value });
				action(parseInt(select.value), (error: string) => setErrorState(error));
				break;
		}

	}

	const showError = () => {
		if (error === "") return;
		return (<div className="error_message">{error}</div>);
	}

	return (
		<div className="modal_container">
			<div className="modal_window">
				<div className="modal_text">
					<h2>Переместить папку</h2>
					{/* {errorModule.message} */}
				</div>
				{
					isLoading
						?
						<div className="sweet_loading" style={{ marginLeft: 'auto', marginRight: 'auto' }}>
							<PulseLoader
								// css={override}
								// size={150}
								color={"#18b5a4"}
								loading={true}
							/>
						</div>
						:
						<>
							{/* <form id="enter_name_form"> */}
							{/* <input type="text" className="form_input_text" id="form_input_text" /> */}
							<select className="form_select" ref={el => select = el!}>
								<option value="">Выберите папку</option>
								<option value="0">Корневая папка</option>
							</select>
							{showError()}
							<div className="modal_buttons form" style={{ justifyContent: 'space-between' }}>
								<button onClick={okButton}>Ок</button>
								<button onClick={cancelFunction}>Отмена</button>
							</div>
							{/* </form> */}
						</>
				}
			</div>
		</div>
	)
}

export default MoveFolderModal;

const filterFolders = (folders: Array<IFolder>, currentFolder: IFolder): Array<number> => {
	let result: Array<number> = [];
	result.push(currentFolder.id);

	currentFolder.folders.forEach(folder => {
		result = result.concat(filterFolders(folders, folder));
	});

	return result;
}