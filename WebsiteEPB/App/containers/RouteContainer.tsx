import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { HomeContainer } from './HomeContainer';
import { LoginContainer } from './LoginContainer';
import { RegisterContainer } from './RegisterContainer';
import { ProfileContainer } from './ProfileContainer';
import { EditProfileContainer } from './EditProfileContainer';
import { ChangePasswordContainer } from './ChangePasswordContainer';
import { WorkbenchContainer } from './workbench/WorkbenchContainer';
import { LINKS } from '../constants/routes';
import { PipeStraightContainer } from './pipeline/PipeStraightContainer'


const RouteContainer: React.FC = () => {
	return (
		
			<Switch>
				<div className="content">
					<Route exact path={LINKS.MAIN} component={HomeContainer} />
					<Route path='/test' component={PipeStraightContainer} />
					<Route path={LINKS.LOGIN} component={LoginContainer} />
					<Route path={LINKS.REGISTER} component={RegisterContainer} />
					<Route exact path={LINKS.PROFILE} component={ProfileContainer} />
					<Route path={LINKS.EDIT_PROFILE} component={EditProfileContainer} />
					<Route path={LINKS.CHANGE_PASSWORD} component={ChangePasswordContainer} />
					<Route path={LINKS.FOLDERS} component={WorkbenchContainer} />
				</div>
			</Switch>
		
	)
}

export default RouteContainer;