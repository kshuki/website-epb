import React from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IAppStore } from '../types';
import { LINKS } from '../constants';
// import { PipeBendContainer } from './Pipeline/PipeBendContainer'

const ProfileContainer = () => {
	const history = useHistory();
	const profile = useSelector((state: IAppStore) => { return state.profileModule });
	if (!profile.isLoggen) {
		history.push(LINKS.LOGIN);
	}

	const editProfileButton = () => history.push(LINKS.EDIT_PROFILE);
	const changePasswordButton = () => history.push(LINKS.CHANGE_PASSWORD);
	const getFolders = () => history.push(LINKS.FOLDERS);

	return (
		<div>
			<div>
				<h2>Profile!!!!!!!!!</h2>
				<br />
				<h3>{profile.firstName} {profile.secondName}</h3>
				<br />
				{profile.status}
				<br />
				<button onClick={editProfileButton}>
					Редактировать профиль
				</button>
				<button onClick={changePasswordButton}>
					Изменить пароль
				</button>
			</div>
			<button onClick={getFolders}>
				Мои файлы
			</button>
			<br />
		</div>
	)
}

export { ProfileContainer };


// const editProfile = () => {
// 	axios.patch(API.EDIT_PROFILE)
// }