import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { LINKS, API } from '../constants';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../actions/constants';
import { IProfile, IAppStore } from '../types';
import { useHistory } from 'react-router-dom';

const LoginContainer = () => {
	const history = useHistory();

	if (useSelector((state: IAppStore) => { return state.profileModule.isLoggen })) {
		history.push(LINKS.PROFILE);
	}

	const [state, setState] = useState({
		isLoading: false,
		email: '',
		password: '',
		isRememberMe: false
	});
	
	const { isLoading, email, password, isRememberMe } = state;

	useEffect(() => {
		if (isLoading) return;
		console.log(state);
		let form: any = document.getElementById("LoginForm");
		form.Email.value = email;
		form.Password.value = password;
		form.RememberMe.checked = isRememberMe;
	})

	const dispatch = useDispatch();

	const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		const form: any = document.getElementById("LoginForm");
		const email = form.Email.value;
		const password = form.Password.value;
		const isRememberMe = form.RememberMe.checked;

		setState({ ...state, isLoading: true, email, password, isRememberMe });

		const dataRequest = {
			email: email,
			password: password,
			remember: isRememberMe
		}

		requestLogin(dispatch, dataRequest, () => setState({ ...state, isLoading: false, email, password, isRememberMe }));
	}

	const routeToRegister = () => {
		history.push(LINKS.REGISTER);
	}

	if (isLoading) {
		return (<div>Идет загрузка</div>)
	}
	return (
		<div className="container">
			<div className="container_form">
				<h2>Вход</h2>
				<form id="LoginForm" onSubmit={onSubmit}>
					{/* <label> */}
						<input type="email" className="form_input_text" placeholder="E-mail" name="Email" required />
					{/* </label> */}
					{/* <label> */}
						<input type="password" className="form_input_text" placeholder="Пароль" name="Password" required />
					{/* </label> */}
					{/* <label> */}
					{/* <div className="container_checkbox"> */}
					<div className="form_checkbox">
						<label className="container_checkbox"> 
							<div>Запомнить меня</div>
							<input type="checkbox" name="RememberMe" value="true" />
							<span className="checkmark"></span>
						</label>
					</div>
						{/* <div>
							Запомнить меня
						</divdiv> */}
					{/* </div> */}
					{/* </div> */}
					<div className="form_buttons">
						<button type="submit">
							Войти
						</button>
						<button onClick={routeToRegister}>
							Зарегистрироваться
						</button>
					</div>
						<a href="#" className="form_link">Забыли пароль?</a>
				</form>
			</div>
		</div>
	)
}

export { LoginContainer };

function requestLogin(dispatch: any, data: any, stopLoading: (() => void)) {
	axios.post(
		API.LOGIN,
		JSON.stringify(data),
		{
			headers:
			{
				"Content-Type": "application/hal+json"
			}
		})
		.then((response: { data: IProfile }) => {
			console.log(response);
			dispatch({ type: actions.LOGIN, payload: response.data })
		})
		.catch(error => {
			if (error.response.status === 401) {
				alert('Нерпавильно ввели пароль или логин');
			} else {
				alert('Неизвестная ошибка, повторите попытку позже');
			}
			console.log("error", error);
			stopLoading();
		})
}