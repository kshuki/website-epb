'use strict';

const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//const HTMLPlugin = require('html-webpack-plugin');

const bundleFolder = "./wwwroot/assets/";
const srcFolder = "./App/"


module.exports = {
	entry: [
		srcFolder + "index.tsx",
		'babel-polyfill'
	],
	devtool: "source-map",
	output: {
		filename: "bundle.js",
		publicPath: 'assets/',
		path: path.resolve(__dirname, bundleFolder)
	},
	module: {
		rules: [
			{
				test: /\.(ts|js)x?$/,
				exclude: /node_modules/,
				loader: "babel-loader"
			},
			{
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					// Creates `style` nodes from JS strings
					'style-loader',
					// Translates CSS into CommonJS
					'css-loader',
					// Compiles Sass to CSS
					'sass-loader',
				],
			},
			{
				test: /\.html$/i,
				loader: 'html-loader',
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				// use: [
					// {
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'img/',
							// publicPath: srcFolder + 'source/',
						},
					// }
				// ],
			},
		]
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.json']
	},
	plugins: [
		new CleanWebpackPlugin(),
		new webpack.ProvidePlugin({

		})

		//new HTMLPlugin({
		//    filename: 'index.html',
		//    template: './App'
		//})
	],
}