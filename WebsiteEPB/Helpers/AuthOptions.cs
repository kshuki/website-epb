﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebsiteEPB.Helpers
{
	public class AuthOptions
	{
		public const string ISSUER = "EPB";
		public const string AUDIENCE = "User";
		const string KEY = "authentification_security_key!qwe123";
		public const int LIFETIME = 60;
		public static SymmetricSecurityKey GetSymmetricSecurityKey()
		{
			return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
		}
	}
}
