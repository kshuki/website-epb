﻿using DBRepository.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DBRepository.Mocks
{
	public class MockFolders : IObjectTree<Folder>
	{
		private List<Folder> _folders = new List<Folder>
		{
			new Folder {
				Id = 1,
				IdProfile = 2,
				IdParent = 0,
				Title = "Папка 1"
			},
			new Folder {
				Id = 2,
				IdProfile = 2,
				IdParent = 0,
				Title = "Папка 2"
			},
			new Folder {
				Id = 3,
				IdProfile = 2,
				IdParent = 1,
				Title = "Вложенная папка 2"
			},
			new Folder {
				Id = 4,
				IdProfile = 2,
				IdParent = 1,
				Title = "Вложенная папка 1"
			},
		};

		public IEnumerable<Folder> GetObjects(uint idProfile)
		{
			var folders = _folders.FindAll(f => f.IdProfile == idProfile);
			return folders;
		}

		public Folder GetObject(uint idFolder)
		{
			var folder = _folders.FirstOrDefault(f => f.Id == idFolder);
			return folder;
		}

		public Folder AddObject(uint idProfile, uint idParent, string title)
		{
			var folder = new Folder
			{
				Id = (_folders.Count > 0) ? _folders.Last().Id + 1 : 1,
				IdParent = idParent,
				Title = title,
				IdProfile = idProfile
			};

			_folders.Add(folder);
			return folder;
		}

		public bool RemoveObject(uint idFolder, uint idProfile)
		{
			var folder = _folders.FirstOrDefault(f => f.Id == idFolder && f.IdProfile == idProfile);
			if (folder == null)
			{
				return false;
			}
			else
			{
				RemoveParent(idFolder);
				return true;
			}
		}

		public void RemoveParent(uint idFolder)
		{
			//TODO Исправить
			_folders.RemoveAll(f => f.Id == idFolder);
		}

		public bool EditObject(Folder folder)
		{
			var index = _folders.IndexOf(_folders.Where(n => n.Id == folder.Id && n.IdProfile == folder.IdProfile).FirstOrDefault());
			if (index == -1)
			{
				return false;
			}
			else
			{
				_folders[index] = folder;
				return true;
			}
		}
	}
}
