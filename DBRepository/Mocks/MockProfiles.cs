﻿using DBRepository.Interfaces;
using Models;
using System.Collections.Generic;
using System.Linq;

namespace DBRepository.Mocks
{
	public class MockProfiles : IProfile<Profile>
	{
		private List<Profile> _users = new List<Profile>
		{
			new Profile { 
				Id = 1, 
				FirstName = "Admin",
				SecondName = "Admin", 
				Email = "admin", Password = "admin", 
				IsAdmin = true
			},
			new Profile { Id = 2,
				FirstName = "Вася",
				SecondName = "Пупкин",
				Email = "t@t.dev", 
				Password = "t", 
				IsAdmin = false
			},
		};
		public IEnumerable<Profile> GetProfiles { get => _users;}

		public void AddProfile(Profile obj)
		{
			_users.Add(obj);
		}

		public Profile GetProfile(string email)
		{
			var user =_users.FirstOrDefault(u => u.Email == email);
			return user;
		}
	}
}
