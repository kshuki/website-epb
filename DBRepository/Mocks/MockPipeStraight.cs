﻿using DBRepository.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBRepository.Mocks
{
    public class MockPipeStraight : IPipePart<PipeStraight>
    {
        private List<PipeStraight> _pipeStraights = new List<PipeStraight>
        {
            new PipeStraight
            {
                Id = 1,
                IdPipe = 2,

                Pressure = 1.0f,
                Temperature = 20,
                OutsideDiameter = 25,

                CurrentThickness = 2.5f,
                RejectionThickness = 1.0f,
                EstimatedThickness = 0.11f,

                WearRate = 0.1f,
                WeldСoefficient = 1.0f,

                IsCustomMaterial = false,
                TitleMaterial = "Ст2кп",
                PermissibleStresses = 124,
                Resource = 0,

                RemainingServiceLife = 15,
            },

            new PipeStraight
            {
                Id = 2,
                IdPipe = 1,

                Pressure = 1.0f,
                Temperature = 20,
                OutsideDiameter = 25,

                CurrentThickness = 2.5f,
                RejectionThickness = 1.0f,
                EstimatedThickness = 0.11f,

                WearRate = 0.1f,
                WeldСoefficient = 1.0f,

                IsCustomMaterial = false,
                TitleMaterial = "Ст2кп",
                PermissibleStresses = 124,
                Resource = 0,

                RemainingServiceLife = 15,
            },

            new PipeStraight
            {
                Id = 3,
                IdPipe = 2,

                Pressure = 1.0f,
                Temperature = 150,
                OutsideDiameter = 57,

                CurrentThickness = 3.5f,
                RejectionThickness = 1.0f,
                EstimatedThickness = 0.23f,

                WearRate = 0.1f,
                WeldСoefficient = 1.5f,

                IsCustomMaterial = false,
                TitleMaterial = "08",
                PermissibleStresses = 125,
                Resource = 0,

                RemainingServiceLife = 20,
            }
        };

        public PipeStraight AddObject(PipeStraight pipeStraight)
        {
            throw new NotImplementedException();
        }

        public bool EditObject(PipeStraight pipeStraight)
        {
            var index = _pipeStraights.IndexOf(_pipeStraights.Where(n => n.Id == pipeStraight.Id).FirstOrDefault());
            if (index == -1)
            {
                return false;
            }
            else
            {
                _pipeStraights[index] = pipeStraight;
                return true;
            }
        }

        public PipeStraight GetObject(uint idPipeStraight)
        {
            var pipe = _pipeStraights.FirstOrDefault(p => p.Id == idPipeStraight);
            return pipe;
        }

        public IEnumerable<PipeStraight> GetObjects(uint idPipe)
        {
            var pipeStraights = _pipeStraights.FindAll(p => p.IdPipe == idPipe);
            return pipeStraights;
        }

        public bool RemoveObject(uint idPipeStraight)
        {
            var pipePart = _pipeStraights.FirstOrDefault(p => p.Id == idPipeStraight);
            if (pipePart == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void RemoveParent(uint idPipe)
        {
            _pipeStraights.RemoveAll(p => p.IdPipe == idPipe);
        }
    }
}