﻿using DBRepository.Interfaces;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBRepository.Mocks
{
	public class MockPipe : IObjectTree<Pipe>
	{
		private List<Pipe> _pipes = new List<Pipe>
		{
			new Pipe
			{
				Id = 1,
				IdParent = 2,
				IdProfile = 2,
				Title = "Трубопровод №1",
				IsBuy = false
			},
			new Pipe
			{
				Id = 2,
				IdParent = 0,
				IdProfile = 2,
				Title = "Трубопровод №2",
				IsBuy = false
			},
		};

		public Pipe AddObject(uint idProfile, uint idFolder, string title)
		{
			var pipe = new Pipe
			{
				Id = (_pipes.Count > 0) ? _pipes.Last().Id + 1 : 1,
				IdParent = idFolder,
				Title = title,
				IdProfile = idProfile,
				IsBuy = false
			};

			_pipes.Add(pipe);
			return pipe;
		}

		public bool EditObject(Pipe pipe)
		{
			var index = _pipes.IndexOf(_pipes.Where(n => n.Id == pipe.Id && n.IdProfile == pipe.IdProfile).FirstOrDefault());
			if (index == -1)
			{
				return false;
			}
			else
			{
				_pipes[index] = pipe;
				return true;
			}
		}

		public Pipe GetObject(uint idPipe)
		{
			var pipe = _pipes.FirstOrDefault(p => p.Id == idPipe);
			return pipe;
		}

		public IEnumerable<Pipe> GetObjects(uint idProfile)
		{
			var pipes = _pipes.FindAll(p => p.IdProfile == idProfile);
			return pipes;
		}

		public bool RemoveObject(uint idPipe, uint idProfile)
		{
			var pipe = _pipes.FirstOrDefault(f => f.Id == idPipe && f.IdProfile == idProfile);
			if (pipe == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public void RemoveParent(uint idFolder)
		{
			_pipes.RemoveAll(p => p.IdParent == idFolder);
		}
	}
}
