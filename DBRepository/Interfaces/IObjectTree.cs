﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBRepository.Interfaces
{
	public interface IObjectTree<T>
	{
		IEnumerable<T> GetObjects(uint idProfile);
		T GetObject(uint idObject);
		T AddObject(uint idProfile, uint idParent, string title);
		bool RemoveObject(uint idObject, uint idProfile);
		void RemoveParent(uint idParent);
		bool EditObject(T obj);
	}
}
