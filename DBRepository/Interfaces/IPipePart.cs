﻿using System.Collections.Generic;

namespace DBRepository.Interfaces
{
    public interface IPipePart<T>
    {
		IEnumerable<T> GetObjects(uint idParent);
		T GetObject(uint idObject);
		T AddObject(T obj);
		bool RemoveObject(uint idObject);
		void RemoveParent(uint idParent);
		bool EditObject(T obj);
	}
}
