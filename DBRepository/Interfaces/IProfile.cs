﻿using System.Collections.Generic;


namespace DBRepository.Interfaces
{
	public interface IProfile<T>
	{
		IEnumerable<T> GetProfiles { get; }
		T GetProfile(string email);
		void AddProfile(T user);
	}
}
